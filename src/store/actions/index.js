import * as types from '../types';

export const showOpenCart = (status) => ({ type: types.SHOW_OPEN_CART, status })