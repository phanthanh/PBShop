import React, { memo, useLayoutEffect, useState } from 'react';
import { Box, Container, Grid, makeStyles } from "@material-ui/core";
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { Navigation, Thumbs } from 'swiper/core';
import Breadcrumb from 'components/Breadcrumb';
import AddToCart from 'containers/AddToCart';
import { formatVND } from 'utils/Util';
import { getProductDetail } from './crud';
import './style.scss';

const useStyle = makeStyles(theme => ({
  root: {
  },
  productTitle: {
    fontSize: 30,
    fontWeight: 500,
    textTransform: 'capitalize',
    margin: '0 0 12px',
    paddingBottom: theme.spacing(1),
    color: theme.palette.text.main,
    lineHeight: '30px',
    borderBottom: '1px solid #eee',
  },
  productPrices: {
    '& .basePrice': {
      fontSize: 22,
      fontWeight: 500,
      color: theme.palette.primary.main
    }
  },
  productFooter: {
    display: 'flex',
    alignItems: 'center',
    borderTop: '1px solid #EEE',
    borderBottom: '1px solid #EEE',
    padding: theme.spacing(2, 0),
    '& input': {
      width: 50,
      borderRadius: 0,
      margin: theme.spacing(0, 2),
      padding: theme.spacing(1, 2),
      height: 'auto',
      border: '1px solid #CCC',
      textAlign: 'center',
      outline: 'unset',
    }
  },
  productExpand: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    padding: theme.spacing(2, 0),
    '& > div': {
      width: '48%',
      border: '1px solid #EEE',
      padding: 5,
      color: '#666',
      textAlign: 'center',
      '& > span': {
        padding: theme.spacing(1),
        display: 'block',
        background: '#F4F4F4',
        marginBottom: 5,
        fontSize: 13
      }
    }
  }
}))

SwiperCore.use([Navigation, Thumbs]);

const ProductDetail = memo(function ({
  // history,
  match,
}) {
  const { productId } = match.params
  const classes = useStyle();
  const [thumbsSwiper, setThumbsSwiper] = useState(null);
  const [data, setData] = useState(null)
  const [qty, setQty] = useState(1)

  useLayoutEffect(() => {
    if (productId) {
      getProductDetail(productId)
        .then(res => {
          if (res.status >= 200 && res.status < 300) {
            setData(res.data)
          }
        })
    }
  }, [productId])

  return !data ? 'Loading...' : (
    <Box className={classes.root}>
      <Breadcrumb data={[
        { url: '', label: data?.name || '' }
      ]} />
      <Container>
        <Grid container spacing={4}>
          <Grid item md={6}>
            <Swiper style={{ '--swiper-navigation-color': '#fff', '--swiper-pagination-color': '#fff' }} spaceBetween={10} thumbs={{ swiper: thumbsSwiper }} className="productGallery">
              {data.images && data.images.map((image, index) => <SwiperSlide key={index}><img src={image} /></SwiperSlide>)}
            </Swiper>
            <Swiper onSwiper={setThumbsSwiper} loop={true} spaceBetween={10} navigation={true} slidesPerView={4} freeMode={true} watchSlidesVisibility={true} watchSlidesProgress={true} className="productThumbs">
              {data.images && data.images.map((image, index) => <SwiperSlide key={index}><img src={image} /></SwiperSlide>)}
            </Swiper>
          </Grid>
          <Grid item md={6}>
            <h1 className={classes.productTitle}>{data.name}</h1>
            <div className={classes.productPrices}>
              <div className="basePrice">{formatVND(data.basePrice)}</div>
            </div>
            <div className={classes.productFooter}>
              <span>Số lượng</span>
              <input type="text" onChange={e => setQty(e.target.value)} value={qty} />
              <AddToCart product={data} qty={qty}>Thêm giỏ hàng</AddToCart>
            </div>
            <div className={classes.productExpand}>
              <div>
                <span>MÃ SẢN PHẨM</span>
                {data.code}
              </div>
              <div>
                <span>ĐIỂM TÍCH LŨY</span>
                1000
              </div>
            </div>
          </Grid>
        </Grid>
      </Container>
    </Box>
  )
})

export default ProductDetail