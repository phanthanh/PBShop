import axios from 'axios'

const URL = 'api/products'

export const getProductDetail = (productId) => {
    return axios.get(`${URL}/${productId}`)
}