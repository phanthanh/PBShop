import React, { memo, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Box, makeStyles } from "@material-ui/core";
import { Swiper, SwiperSlide } from 'swiper/react';
import BlockTitle from 'components/BlockTitle';
import Product from 'components/Product';
import BaseButton from 'components/BaseButton';
import { getInventoryProducts } from './crud';

const useStyle = makeStyles(theme => ({
  root: {
    '& .swiper-button-prev, & .swiper-button-next': {
      background: theme.palette.primary.main,
      width: 30,
      height: 30,
      borderRadius: '50%',
      transition: 'all 300ms',
      '&:after': {
        display: 'none'
      },
      '& span': {
        fontSize: 30,
        color: theme.palette.common.white
      },
      '&:hover': {
        background: theme.palette.secondary.main
      }
    }
  }
}))

export default memo(function () {
  const classes = useStyle()
  const history = useHistory()
  // const prevRef = useRef(null);
  // const nextRef = useRef(null);
  const [list, setList] = useState([])

  useEffect(() => {
    getInventoryProducts({ limit: 12 })
      .then(res => {
        if (res.status >= 200 && res.status < 300) {
          setList(res.data.data)
        }
      })
  }, [])

  return (
    <div className={classes.root}>
      <BlockTitle variant="h2">Sản phẩm bán chạy</BlockTitle>
      <Swiper
        // slidesPerView={4}
        slidesPerColumn={3}
        spaceBetween={30}
        slidesPerColumnFill="row"
        // navigation={{
        //   prevEl: prevRef?.current,
        //   nextEl: nextRef?.current,
        // }}
        // onInit={(swiper) => {
        //   swiper.params.navigation.prevEl = prevRef.current;
        //   swiper.params.navigation.nextEl = nextRef.current;
        //   swiper?.navigation?.update();
        // }}
        breakpoints={{
          480: {
            slidesPerView: 2,
            spaceBetween: 20,
            slidesPerColumn: 3,
          },
          960: {
            slidesPerView: 4,
            spaceBetween: 30,
            slidesPerColumn: 3,
          }
        }}
      >
        {
          list.map((item, i) => (
            <SwiperSlide key={i}>
              <Product data={item} />
            </SwiperSlide>
          ))
        }
        {/* <div className="swiper-button-prev" ref={prevRef}><span className="ico-arrow-left" /></div>
        <div className="swiper-button-next" ref={nextRef}><span className="ico-arrow-right" /></div> */}
      </Swiper >
      <Box display="flex" justifyContent="center" my={2}>
        <BaseButton
          color="primary"
          variant="contained"
          onClick={() => history.push('/tim-kiem')}
        >
          Xem thêm</BaseButton>
      </Box>
    </div>
  )
})