import axios from 'axios'

export const getArrivalProducts = () => {

}

export const getLatestProducts = () => {

}

export const getInventoryProducts = (params) => {
    return axios.get(`api/products/inventory`, {
        params
    })
}

export const getProductCategories = (params) => {
    return axios.get(`api/product-categories`, {
        params
    })
}