import React from 'react'
// import loadable from "@loadable/component";
import { Box, Container } from "@material-ui/core";
import ProductCategory from './ProductCategory';
import Products from './Products';

// const Banner = loadable(() => import("components/Banner"));

function Home() {
  return (
    <>
      {/* <Banner /> */}
      <Container>
        <ProductCategory />
        <Box py={3} />
        <Products />
      </Container>
    </>
  )
}

export default Home