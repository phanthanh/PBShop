import React, { memo, useEffect, useState } from "react";
import { Box, makeStyles } from "@material-ui/core";
import { Swiper, SwiperSlide } from 'swiper/react';
import { useHistory } from "react-router";
import slugify from 'slugify';
import BlockTitle from 'components/BlockTitle';
import Title from 'components/Title';
import ProductCategoryImg from 'assets/images/4-160x160.png';
import { getProductCategories } from "./crud";

const useStyle = makeStyles(theme => ({
  root: {
    padding: '0 50px',
    '& .swiper-wrapper': {
      justifyContent: 'center'
    }
  },
  item: {
    position: 'relative',
    textAlign: 'center',
    paddingBottom: theme.spacing(1),
    cursor: 'pointer',
    '& img': {
      width: "auto",
      maxWidth: "100%",
      margin: "5px auto 0",
      borderRadius: "50%",
      transition: "all 0.5s"
    },
    '& .MuiTypography-h4': {
      transition: 'all .5s'
    },
    '&:before': {
      content: "''",
      border: "1px solid #eee",
      borderTop: "0",
      position: "absolute",
      left: "0",
      right: "0",
      top: "56%",
      bottom: "0",
      borderRadius: "0 0 15px 15px"
    },
    '&:after': {
      content: "''",
      height: "40px",
      width: "100%",
      position: "absolute",
      left: "0",
      top: "41%",
      right: "0",
      textAlign: "center",
      margin: "0 auto",
      zIndex: "-1",
      border: "1px solid #eee",
      borderBottom: "0",
      borderRadius: "100%/100px 100px 0 0"
    },
    '&:hover': {
      '& img': {
        transform: 'rotate(4deg)'
      },
      '& .MuiTypography-h4': {
        color: theme.palette.primary.main,
        transform: 'rotateX(360deg)'
      }
    }
  }
}))

export default memo(function ProductCategory() {
  const classes = useStyle()
  const history = useHistory()
  const [list, setList] = useState([])

  useEffect(() => {
    getProductCategories({ limit: 4 })
      .then(res => {
        if (res.status >= 200 && res.status < 300) {
          setList(res.data.data)
        }
      })
  }, [])

  return (
    <div className={classes.root}>
      <BlockTitle variant="h2">Cho bé mặc</BlockTitle>
      < Swiper
        spaceBetween={50}
        slidesPerView={6}
        breakpoints={{
          0: {
            slidesPerView: 3,
            spaceBetween: 30,
          },
          960: {
            slidesPerView: 6,
            spaceBetween: 50,
          }
        }}
      >
        {
          list.map((row, i) => {
            const slug = slugify(row.name)
            return (
              <SwiperSlide key={i}>
                <Box className={classes.item} onClick={() => history.push(`/${slug}/c${row.id}`)}>
                  <img src={ProductCategoryImg} alt={row.name} />
                  <Title variant="h4">{row.name}</Title>
                </Box>
              </SwiperSlide>
            )
          })
        }
      </Swiper >
    </div>
  )
})
