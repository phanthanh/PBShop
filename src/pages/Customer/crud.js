import axios from "axios"

export const getCustomerDeliveries = () => {
  return axios.get(`api/deliveries`)
    .catch(err => console.log(err))
}

export const addCustomerDelivery = data => {
  return axios.post(`api/deliveries`, data)
    .catch(err => console.log(err))
}

export const updateCustomerDelivery = (id, data) => {
  return axios.put(`api/deliveries/${id}`, data)
    .catch(err => console.log(err))
}

export const deleteCustomerDelivery = id => {
  return axios.delete(`api/deliveries/${id}`)
    .catch(err => console.log(err))
}

export const getProvinces = (params) => {
  return axios.get(`master-data/provinces`, { params })
    .catch(err => console.log(err))
}

export const getDistricts = (params) => {
  return axios.get(`master-data/districts`, { params })
    .catch(err => console.log(err))
}
