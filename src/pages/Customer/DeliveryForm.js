import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { Box, FormHelperText, makeStyles } from '@material-ui/core'
import { Field, getFormValues, reduxForm } from 'redux-form'
import ReduxTextField from 'components/ReduxTextField'
import ReduxSelectField from 'components/ReduxSelectField'
import Button from 'components/BaseButton'
import { addCustomerDelivery, getProvinces, getDistricts } from './crud'

const useStyles = makeStyles(theme => ({
  root: {

  },
  item: {
    margin: theme.spacing(1)
  },
  footer: {
    paddingRight: 6,
    paddingLeft: 6,
    display: 'flex',
    justifyContent: 'space-between'
  }
}))

const validate = values => {
  const errors = {}
  const requiredFields = ['fullName', 'phone', 'provinceId', 'districtId', 'address']
  requiredFields.forEach(field => {
    if (!values[field] || (typeof values[field] === 'string' && !values[field].trim())) {
      errors[field] = 'Không được để trống'
    }
  })
  return errors
}

function DeliveryForm({
  handleSubmit,
  pristine,
  reset,
  submitting,
  formValues,
  initialValues,
  onClose,
  onSuccess,
}) {
  const classes = useStyles()
  const isNew = !initialValues || (initialValues && !initialValues.id)
  const [error, setError] = useState(null)
  const [provinces, setProvinces] = useState([])
  const [districts, setDistricts] = useState([])

  useEffect(() => {
    getProvinces({ limit: 64 })
      .then(res => {
        if (res?.status === 200) {
          setProvinces(res.data.data)
        }
      })
  }, [])

  useEffect(() => {
    if (formValues?.provinceId) {
      getDistricts({ limit: 99, provinceId: formValues.provinceId })
        .then(res => {
          if (res?.status === 200) {
            setDistricts(res.data.data)
          }
          else {
            setDistricts([])
          }
        })
    }
    else {
      setDistricts([])
    }
  }, [formValues?.provinceId])

  const onSubmit = values => {
    addCustomerDelivery(values)
      .then(res => {
        if (res?.status >= 200 && res?.status < 300) {
          reset()
          if (onSuccess) {
            onSuccess(res.data.data)
          }
        } else {
          setError('Có lỗi xảy ra. Vui lòng thử lại sau')
        }
      })
  }

  return (
    <form className={classes.root}>
      <Box className={classes.item}>
        <Field
          component={ReduxTextField}
          name="fullName"
          label="Họ và tên người nhận *"
          variant="standard"
        />
      </Box>
      <Box className={classes.item}>
        <Field
          component={ReduxTextField}
          name="phone"
          label="Số điện thoại người nhận *"
        />
      </Box>
      <Box className={classes.item}>
        <Field
          component={ReduxSelectField}
          name="provinceId"
          label="Tỉnh/Thành phố *"
          list={provinces}
        />
      </Box>
      <Box className={classes.item}>
        <Field
          component={ReduxSelectField}
          name="districtId"
          label="Quận/Huyện *"
          list={districts}
        />
      </Box>
      <Box className={classes.item}>
        <Field
          component={ReduxTextField}
          name="address"
          label="Địa chỉ cụ thể *"
        />
      </Box>
      {error && <FormHelperText error>{error}</FormHelperText>}
      <div className="SidebarDialog-Footer">
        <Box className={classes.footer}>
          {formValues ? (
            <Button
              color="primary"
              size="large"
              variant="outlined"
              onClick={reset}
              disabled={pristine || submitting}
            >
              {isNew ? 'Làm mới' : 'Khôi phục'}
            </Button>
          ) : (
            <Button
              color="primary"
              size="large"
              variant="outlined"
              onClick={onClose}
            >
              Đóng
            </Button>
          )}
          <Button
            color="primary"
            size="large"
            variant="contained"
            onClick={handleSubmit(onSubmit)}
            disabled={pristine || submitting}>
            {isNew ? 'Thêm' : 'Cập nhật'}
          </Button>
        </Box>
      </div>
    </form>
  )
}

const Form = reduxForm({
  form: 'DeliveryForm',
  validate,
})(DeliveryForm)

export default connect(state => ({
  formValues: getFormValues('DeliveryForm')(state)
}))(Form)