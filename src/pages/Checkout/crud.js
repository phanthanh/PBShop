import axios from "axios"
import { formatErrorResponse } from "utils/Util"

const URL = 'api/checkouts'

export const customerCheckout = (data) => {
  return axios.post(URL, data)
    .catch(err => formatErrorResponse(err))
}
