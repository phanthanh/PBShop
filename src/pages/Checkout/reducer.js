import produce from 'immer';

const key = '@Checkout'

export const types = {
  UPDATE_CART: key + '/UPDATE_CART',
  TOGGLE_CART: key + '/TOGGLE_CART',
}

export const actions = {
  toggleCart: status => ({ type: types.TOGGLE_CART, status }),
  updateCart: payload => ({ type: types.UPDATE_CART, payload }),
}

export const initialState = {
  showCart: false,
  items: [],
}

const checkoutReducer = (state = initialState, action) =>
  produce(state, draft => {

    switch (action.type) {

      case types.UPDATE_CART:
        draft.items = action.payload
        break;

      case types.TOGGLE_CART:
        draft.showCart = action.status
        break;

      default:
        break;
    }
  })

export default checkoutReducer
