import React, { useState, useLayoutEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { Box, Container, lighten, makeStyles } from '@material-ui/core'
import { red } from '@material-ui/core/colors'
import BaseButton from 'components/BaseButton'
import BIDVLogo from 'assets/images/BIDV_Logo.png'
import TechLogo from 'assets/images/Techcombank_logo.png'

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    '& .danger': {
      color: red[500]
    },
    '& h2': {
      textAlign: 'center',
      color: theme.palette.secondary.main,
      margin: theme.spacing(2, 0)
    },
    '& .content': {
      fontWeight: 500
    }
  },
  item: {
    display: 'flex',
    alignItems: 'center',
    border: `1px dashed rgba(0,0,0,0.1)`,
    backgroundColor: lighten(theme.palette.primary.main, 10),
    borderRadius: 4,
    padding: theme.spacing(1, 2),
    margin: theme.spacing(2, 0),
    '& > img': {
      marginRight: theme.spacing(2),
      maxWidth: 100,
    },
    '& > div': {
      '& div': {
        display: 'flex',
        padding: theme.spacing(0.3, 0),
        '& span': {
          display: 'inline-block',
          width: 100,
          marginRight: theme.spacing(2),
        }
      }
    }
  }
}))

export default function OrderSuccess() {
  const classes = useStyles()
  const history = useHistory()
  const [banks, setBanks] = useState([])

  useLayoutEffect(() => {
    setBanks([
      {
        logo: BIDVLogo,
        name: 'BIDV - Ngân hàng TMCP Đầu tư và Phát triển Việt Nam',
        account: '21710000048878',
        fullName: 'Tran Thi Huong Thao',
      },
      {
        logo: TechLogo,
        name: 'Techcombank - Ngân hàng thương mại cổ phần Kỹ Thương Việt Nam',
        account: '19034355482017',
        fullName: 'Tran Thi Huong Thao',
      },
    ])
  }, [])

  return (
    <Container>
      <Box className={classes.root}>
        <Box display="inline-block" textAlign="left">
          <h2>Đặt hàng thành công</h2>
          <p>Bạn đã đặt đơn hàng mã <strong></strong> trị giá <strong></strong>, thanh toán chuyển khoản.</p>
          <p className="danger">Lưu ý: Đơn hàng sẽ tự động hủy sau <strong>04 ngày</strong> nếu bạn không chuyển khoản</p>
          <p>Vui lòng thực hiện chuyển khoản với nội dung <span className="content">{`<Họ và tên> <Số điện thoại> <Mã đơn hàng>`}</span></p>
          <p>Chúng tôi sẽ liên hệ với bạn trong vòng 24h sau khi tiền được chuyển tới các số tài khoản dưới đây:</p>
          {banks.map((bank, i) => (
            <Box key={i} className={classes.item}>
              <img src={bank.logo} alt={bank.name} />
              <Box>
                <Box><span>Tên tài khoản:</span> {bank.fullName}</Box>
                <Box><span>Số tài khoản:</span> {bank.account}</Box>
                <Box><span>Ngân hàng:</span> {bank.name}</Box>
              </Box>
            </Box>
          ))}
        </Box>
      </Box>
      <Box display="flex" justifyContent="center" mt={2}>
        <BaseButton onClick={() => history.push('/')} color="primary" variant="contained">Tiếp tục mua hàng</BaseButton>
      </Box>
    </Container>
  )
}