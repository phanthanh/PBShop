import React, { useState, useLayoutEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { useSelector, useDispatch } from "react-redux"
import { Box, Container, Grid, makeStyles, Table, TableBody, TableCell, TableHead, TableRow, Paper, TableContainer, TableFooter, Radio, FormControlLabel, RadioGroup, FormLabel } from '@material-ui/core'
import Breadcrumb from 'components/Breadcrumb'
import Button from 'components/BaseButton'
import QuantityInput from 'components/QuantityInput'
import BaseDialog from 'components/BaseDialog'
import { formatVND } from 'utils/Util'
import { getCustomerDeliveries } from 'pages/Customer/crud'
import DeliveryForm from 'pages/Customer/DeliveryForm'
import { actions } from './reducer'
import { customerCheckout } from './crud'
import OrderSuccess from './OrderSuccess'

const useStyles = makeStyles(theme => ({
  root: {
    '& .MuiTableCell-root': {
    },
    '& td .ico-delete_forever': {
      color: '#FF0000',
      cursor: 'pointer',
    }
  },
  footer: {
    '& .MuiTableCell-footer': {
      fontSize: 16,
      fontWeight: 500,
      color: theme.palette.text.main
    }
  },
  list: {
    margin: theme.spacing(4, 0),
    '& h3': {
      color: theme.palette.text.main,
      marginBottom: theme.spacing(1)
    }
  },
}))

const selectAuth = state => state.auth
const selectCheckout = state => state.checkout

function CheckoutPage() {
  const classes = useStyles()
  const history = useHistory()
  const dispatch = useDispatch()
  const auth = useSelector(selectAuth)
  const checkout = useSelector(selectCheckout)
  const [deliveries, setDeliveries] = useState([])
  const [selected, setSelected] = useState(null)
  const [submitted, setSubmitted] = useState(false)
  const [checkoutSuccess, setCheckoutSuccess] = useState(false)
  const [showDialog, setShowDialog] = useState(false)

  useLayoutEffect(() => {
    if (auth.authenticated) {
      getCustomerDeliveries()
        .then(res => {
          const list = res?.data?.data || []
          setDeliveries(list)
          const defaultDelivery = list.find(item => item.isDefault)
          if (defaultDelivery) {
            setSelected(defaultDelivery.id)
          }
        })
    } else {
      history.push('/')
    }
  }, [])

  const handleSubmit = () => {
    setSubmitted(true)
    const data = {
      deliveryId: selected,
      paymentMethod: 'cash',
      products: checkout.items.map(item => ({ ...item, quantity: Number(item.quantity), price: item.salePrice || item.basePrice })),
      totalAmount: checkout.items.reduce((a, b) => a + b.basePrice * b.quantity, 0),
    }
    customerCheckout(data)
      .then(res => {
        if (res.status >= 200 && res.status < 300) {
          setCheckoutSuccess(true)
          dispatch(actions.updateCart([]))
        }
      })
  }

  const handleChangeQuantity = (productId, quantity) => {
    const items = checkout.items.map(item => item.id === productId ? { ...item, quantity } : item)
    dispatch(actions.updateCart(items))
  }

  const handleRemove = (productId) => {
    const items = checkout.items.filter(item => item.id !== productId)
    dispatch(actions.updateCart(items))
  }

  const handleDeliverySuccess = (delivery) => {
    setShowDialog(false)
    setSelected(delivery.id)
  }

  return (
    <>
      <Breadcrumb data={[
        { label: checkoutSuccess ? 'Thanh toán thành công' : 'Thanh toán đơn hàng' }
      ]} />
      <Container>
        {checkoutSuccess ? (
          <OrderSuccess />
        ) : (
          <Grid container spacing={4}>
            <Grid item xs={3}>

            </Grid>
            <Grid item xs={9}>
              {auth.authenticated && (
                <Box className={classes.root}>
                  <TableContainer component={Paper}>
                    <Table>
                      <TableHead>
                        <TableRow>
                          <TableCell>Ảnh</TableCell>
                          <TableCell>Tên sản phẩm</TableCell>
                          <TableCell>Đơn giá</TableCell>
                          <TableCell>Số lượng</TableCell>
                          <TableCell align="right">Thành tiền</TableCell>
                          <TableCell align="right"><span className="ico-delete_forever" /></TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        {checkout.items.map(item => (
                          <TableRow key={item.id}>
                            <TableCell>
                              <img height={100} src={item.images ? item.images[0] : ''} />
                            </TableCell>
                            <TableCell>
                              <span>{item.name}</span>
                            </TableCell>
                            <TableCell>
                              <span>{formatVND(item.basePrice)}</span>
                            </TableCell>
                            <TableCell>
                              <QuantityInput
                                quantity={item.quantity}
                                maxQty={item.ProductQuantity?.quantity}
                                onChange={val => handleChangeQuantity(item.id, val)}
                              />
                            </TableCell>
                            <TableCell align="right">
                              <spam>{formatVND(item.basePrice * item.quantity)}</spam>
                            </TableCell>
                            <TableCell align="right">
                              <span className="ico-delete_forever" onClick={() => handleRemove(item.id)} />
                            </TableCell>
                          </TableRow>
                        ))}
                      </TableBody>
                      <TableFooter className={classes.footer}>
                        <TableRow >
                          <TableCell colSpan={4}>Tổng tiền</TableCell>
                          <TableCell align="right">{formatVND(checkout.items.reduce((a, b) => a + b.basePrice * b.quantity, 0))}</TableCell>
                          <TableCell />
                        </TableRow>
                      </TableFooter>
                    </Table>
                  </TableContainer>
                  <Box className={classes.list}>
                    <FormLabel component="h3">Chọn địa chỉ giao hàng:</FormLabel>
                    <RadioGroup name="delivery" value={selected} onChange={e => setSelected(e.target.value)}>
                      {deliveries.map((delivery, i) => {
                        const label = (
                          <>
                            <span className={delivery.type === 'office' ? "ico-local_post_office" : "ico-home1"} />
                            {' '}
                            <span>{delivery.fullName} - {delivery.phone} - {delivery.address}</span>
                          </>
                        )
                        return (
                          <FormControlLabel key={i} value={delivery.id} control={<Radio size="small" />} label={label} />
                        )
                      })}
                    </RadioGroup>

                    {deliveries?.length < 5 && (
                      <Button
                        variant="text"
                        color="primary"
                        size="small"
                        startIcon={<span className="ico-add_circle" />}
                        onClick={() => setShowDialog(true)}>
                        Thêm địa chỉ mới</Button>
                    )}
                  </Box>
                  <Box display="flex" justifyContent="center">
                    <Button
                      variant="outlined"
                      color="primary"
                      size="large"
                      onClick={() => history.push('/')}
                    >Tiếp tục mua hàng</Button>
                    <Button
                      variant="contained"
                      color="primary"
                      size="large"
                      onClick={handleSubmit}
                      disabled={submitted}
                      initStyle={{ width: 168, marginLeft: 32 }}
                    >Đặt hàng</Button>
                  </Box>
                </Box>
              )}
            </Grid>
          </Grid>
        )}

        <BaseDialog
          title="Thêm địa chỉ nhận hàng"
          open={showDialog}
          onClose={() => setShowDialog(false)}
        >
          <DeliveryForm onClose={() => setShowDialog(false)} onSuccess={handleDeliverySuccess} />
        </BaseDialog>
      </Container>
    </>
  )
}

export default CheckoutPage