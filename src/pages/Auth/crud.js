import axios from "axios"

const URL = 'api/auth'

export const register = (data) => {
    return axios.post(`${URL}/register`, data)
        .catch(err => console.log(err))
}

export const login = (data) => {
    return axios.post(`${URL}/login`, data)
        .catch(err => console.log(err))
}

export const logout = () => {
    return axios.post(`${URL}/logout`)
        .catch(err => console.log(err))
}


