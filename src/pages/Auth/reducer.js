import produce from 'immer'
import moment from 'moment'
import jsonwebtoken from 'jsonwebtoken'

const key = '@Auth'

const types = {
  AUTHENTICATED: key + '/AUTHENTICATED',
  LOGOUT: key + '/LOGOUT',
  TOGGLE_LOGIN_FORM: key + '/TOGGLE_LOGIN_FORM',
  TOGGLE_REGISTER_FORM: key + '/TOGGLE_REGISTER_FORM',
}

export const initialState = {
  authenticated: false,
  accessToken: null,
  refreshToken: null,
  expiredAt: 0,
  userInfo: null,
  openLoginForm: false,
  openRegisForm: false,
}

export const actions = {
  loginSuccess: payload => ({ type: types.AUTHENTICATED, payload }),
  toggleLoginForm: status => ({ type: types.TOGGLE_LOGIN_FORM, status }),
  toggleRegisterForm: status => ({ type: types.TOGGLE_REGISTER_FORM, status }),
  logout: () => ({ type: types.LOGOUT }),
}

const authReducer = (state = initialState, action) =>
  produce(state, draft => {

    switch (action.type) {

      case types.AUTHENTICATED:
        draft.authenticated = true
        draft.accessToken = action.payload.access_token
        draft.refreshToken = action.payload.refresh_token
        draft.userInfo = jsonwebtoken.decode(action.payload.access_token)
        draft.expiredAt = moment().add(action.payload.exp, 'seconds').format()
        draft.openLoginForm = false
        break;
      
      case types.LOGOUT:
        return initialState;

      case types.TOGGLE_LOGIN_FORM:
        draft.openLoginForm = action.status
        break;
      
      case types.TOGGLE_REGISTER_FORM:
        draft.openRegisForm = action.status
        break;

      default:
        break;
    }
  })

export default authReducer
