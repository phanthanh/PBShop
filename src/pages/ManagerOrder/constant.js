import moment from 'moment';
import { formatVND } from 'utils/Util';

export const columns = [
  {
    title: 'Mã đặt hàng',
    dataIndex: 'code',
    key: 'code',
  },
  {
    title: 'Thời gian',
    dataIndex: 'createdAt',
    key: 'createdAt',
    render: date => moment(date).format('DD/MM/YYYY HH:mm')
  },
  {
    title: 'Khách hàng',
    dataIndex: 'Member',
    key: 'Member',
    render: Member => Member.fullName,
  },
  {
    title: 'Số tiền',
    dataIndex: 'totalAmount',
    key: 'totalAmount',
    render: amount => formatVND(amount),
    align: 'right'
  },
  {
    title: 'Trạng thái',
    dataIndex: 'isActive',
    key: 'isActive',
    render: isActive => isActive ? 'Đã xử lý' : 'Chờ xử lý',
  },
]