import axios from 'axios'
import { formatErrorResponse } from 'utils/Util'

export const getOrders = params => {
  return axios.get('admin/orders', { params })
  .catch(err => formatErrorResponse(err))
}
