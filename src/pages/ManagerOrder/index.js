import React, { useEffect, useState } from 'react'
import { Table } from 'antd'
import { columns } from './constant'
import { getOrders } from './crud'

function ManagerOrder() {
console.log('xx');
  const [listOrders, setListOrders] = useState([])
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    setLoading(true)
    getOrders({})
      .then(res => {
        if (res?.status === 200) {
          setListOrders(res.data.data)
        }
        setLoading(false)
      })
  }, [])

  return loading ? 'Loading...' : (
    <>
      <Table dataSource={listOrders} columns={columns} />
    </>
  )
}

export default ManagerOrder
