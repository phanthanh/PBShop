import React, { useState, useLayoutEffect } from 'react'
import { useHistory, withRouter } from 'react-router-dom'
import Product from 'components/Product'
import { Box, Button, Grid, TextField } from '@material-ui/core'
import Pagination from '@material-ui/lab/Pagination'
import { getInventoryProducts } from './crud'
import { listStyle } from './style'

let deplaySearch

function List({ location }) {
  const classes = listStyle()
  const history = useHistory()
  const [viewType, setViewType] = useState('grid')
  const [list, setList] = useState([])
  const [totalPages, setTotalPages] = useState(0)
  const [defaultPage, setDefaultPage] = useState(1)
  const [rowsPerPage, setRowsPerPage] = useState(12)
  const [order, setOrder] = useState('')
  const [name, setName] = useState('')

  useLayoutEffect(() => {
    const search = new URLSearchParams(location.search)
    const page = search.get('page')
    const keyword = search.get('keyword')
    const sort = search.get('sort')
    const limit = search.get('limit')
    setName(keyword || '')
    setDefaultPage(Number(page) || 1)
    setRowsPerPage(limit || 12)
    setOrder(sort || '')
    handleSubmit({ keyword, page, limit, sort })
  }, [location])

  function handleSubmit(params) {
    getInventoryProducts(params)
      .then(res => {
        if (res?.status >= 200 && res?.status < 300) {
          setList(res.data.data)
          setTotalPages(Math.ceil(res.data.total / 12))
        }
      })
  }

  const handleChangePage = (e, page) => {
    const searchParams = new URLSearchParams(location.search);
    if ('URLSearchParams' in window) {
      searchParams.set("page", page);
    }
    history.push(`/tim-kiem?${searchParams}`)
  }

  const handleSearch = e => {
    const { name, value } = e.target
    const searchParams = new URLSearchParams(location.search);
    searchParams.set(name, value);
    history.push(`/tim-kiem?${searchParams}`)
  }

  const handleKeyword = (e) => {
    const { value } = e.target
    setName(value)
    searchKeyword(value)
  }

  function searchKeyword(str) {
    clearTimeout(deplaySearch)
    deplaySearch = setTimeout(() => {
      const searchParams = new URLSearchParams(location.search);
      if ('URLSearchParams' in window) {
        searchParams.set("keyword", str)
      }
      history.push(`/search?${searchParams}`)
    }, 300);
  }

  return (
    <Box className={classes.root}>
      <form className={classes.form}>
        <Box display="flex" justifyContent="space-between" mb={2} mx={-1}>
          <Box width="100%" px={1}>
            <TextField
              label="Nhập tên hoặc mã sản phẩm"
              name="name"
              variant="outlined"
              value={name}
              onChange={handleKeyword}
            />
          </Box>
        </Box>
      </form>
      <Box className={classes.listTop}>
        <div className="btn-list-grid">
          <Button className={viewType === 'grid' ? 'active' : ''} onClick={() => setViewType('grid')}><span className="ico-grid_view" /></Button>
          <Button className={viewType === 'list' ? 'active' : ''} onClick={() => setViewType('list')}><span className="ico-list" /></Button>
        </div>
        <div className="pagination-right">
          <div className="sort">
            <span>Sắp xếp theo</span>
            <select
              name="sort"
              value={order}
              onChange={handleSearch}
            >
              <option value="">{'Sắp xếp theo'}</option>
              <option value={'priceLowest'}>{'Giá thấp nhất'}</option>
              <option value={'priceHighest'}>{'Giá cao nhất'}</option>
            </select>
          </div>
          <div className="rows-per-page">
            <span>Hiển thị</span>
            <select
              name="limit"
              value={rowsPerPage}
              onChange={handleSearch}
            >
              <option value={12}>12</option>
              <option value={24}>24</option>
            </select>
          </div>
        </div>
      </Box>
      <Box className={classes.productList}>
        <Grid container spacing={4}>
          {list.length > 0 && (
            <>
              {list.map((item, index) => (
                <Grid key={index} item xs={viewType === 'grid' ? 4 : 12}>
                  <Product data={item} horizontal={viewType === 'list'} />
                </Grid>
              ))}
            </>
          )}
        </Grid>
      </Box>
      <Box display="flex" justifyContent="center" mt={4}>
        <Pagination
          color="primary"
          className={classes.pagination}
          defaultPage={defaultPage}
          count={totalPages}
          onChange={handleChangePage} />
      </Box>
    </Box>
  )
}

export default withRouter(List)