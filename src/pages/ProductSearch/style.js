import { makeStyles } from "@material-ui/core";

export const listStyle = makeStyles(theme => ({
  form: {
    marginBottom: theme.spacing(2),
    '& .MuiTextField-root': {
      width: '100%',
      outline: 'none',
    },
    '& .MuiInputBase-root': {
      borderRadius: 0
    },
  },
  submit: {
    opacity: 1,
    fontWeight: 500,
    padding: "7px 20px",
    textTransform: "uppercase",
    borderRadius: 50,
    transition: "all 0.5s",
    color: theme.palette.common.white,
    backgroundColor: "#000",
    border: "1px solid #000",
    boxShadow: "0 2px 4px 0 rgb(0 0 0 / 20%)",
    outline: "none",
    lineHeight: "20px",
    '&:hover, &:focus': {
      backgroundColor: theme.palette.primary.main,
      borderColor: theme.palette.primary.main,
    }
  },
  listTop: {
    padding: "14px 15px",
    lineHeight: "30px",
    backgroundColor: "#f5f5f5",
    marginBottom: theme.spacing(2),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    '& .btn-list-grid': {
      '& button': {
        padding: 0,
        minWidth: 0,
        '&:first-child': {
          marginRight: theme.spacing(0.5)
        },
        '& span': {
          fontSize: 26,
          opacity: 0.65,
          '&.ico-list': {
            fontSize: 40
          },
        },
        '&.active': {
          '& span': {
            opacity: 1
          }
        }
      }
    },
    '& .pagination-right': {
      display: 'flex',
      alignItems: 'center',
      '& .MuiInputBase-root': {
        borderRadius: 0,
        background: theme.palette.common.white
      },
      '& .MuiSelect-root': {
        padding: theme.spacing(.5),
        minWidth: 50,
        borderRadius: 0,
      },
      '& .sort, & .rows-per-page': {
        display: 'flex',
        alignItems: 'center',
        '& select': {
          borderColor: '#DDD',
          padding: theme.spacing(1),
          outline: 'none',
        },
        '& span': {
          display: 'inline-block',
          marginRight: theme.spacing(1),
          color: theme.palette.text.gray,
        }
      },
      '& .sort': {
        marginRight: theme.spacing(2),
      },
      '& .rows-per-page': {

      }
    }
  },
  pagination: {
    '& .MuiPaginationItem-root.Mui-selected': {
      color: theme.palette.common.white
    }
  }
}))
