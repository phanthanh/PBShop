import axios from 'axios'

const URL = 'api/products'

export const getInventoryProducts = (params) => {
    return axios.get(`${URL}/inventory`, {
        params: { ...params, limit: params.limit || 12 }
    })
}
