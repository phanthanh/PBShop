import React, { memo } from 'react';
import { Box, Container, Grid } from "@material-ui/core";
import Breadcrumb from 'components/Breadcrumb';
import SideBar from 'containers/SideBar';
import List from './List';

const ProductSearch = memo(function () {
  return (
    <Box>
      <Breadcrumb data={[
        { label: 'Tìm kiếm' }
      ]} />
      <Container>
        <Grid container spacing={4}>
          <Grid item xs={3}>
            <SideBar />
          </Grid>
          <Grid item xs={9}>
            <List />
          </Grid>
        </Grid>
      </Container>
    </Box>
  )
})

export default ProductSearch