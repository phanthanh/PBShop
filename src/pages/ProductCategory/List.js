import React, { useState, useLayoutEffect } from 'react'
import { useHistory, withRouter } from 'react-router-dom'
import { Box, Button, Grid } from '@material-ui/core'
import Pagination from '@material-ui/lab/Pagination'
import Product from 'components/Product'
import { getInventoryProducts } from 'pages/ProductSearch/crud'
import { listStyle } from 'pages/ProductSearch/style'

function List({ location, match }) {
  const classes = listStyle()
  const history = useHistory()
  const { categoryId } = match.params
  const [viewType, setViewType] = useState('grid')
  const [list, setList] = useState([])
  const [totalPages, setTotalPages] = useState(0)
  const [defaultPage, setDefaultPage] = useState(1)
  const [rowsPerPage, setRowsPerPage] = useState(12)
  const [order, setOrder] = useState('')

  useLayoutEffect(() => {
    const search = new URLSearchParams(location.search)
    const page = search.get('page')
    const sort = search.get('sort')
    const limit = search.get('limit')
    setDefaultPage(Number(page) || 1)
    setRowsPerPage(limit || 12)
    setOrder(sort || '')
    handleSubmit({ page, limit, sort })
  }, [location])

  function handleSubmit(params) {
    getInventoryProducts({ ...params, categoryId })
      .then(res => {
        if (res?.status >= 200 && res?.status < 300) {
          setList(res.data.data)
          setTotalPages(Math.ceil(res.data.total / 12))
        }
      })
  }

  const handleChangePage = (e, page) => {
    const searchParams = new URLSearchParams(location.search);
    if ('URLSearchParams' in window) {
      searchParams.set("page", page);
    }
    history.push(`${location.pathname}?${searchParams}`)
  }

  const handleSearch = e => {
    const { name, value } = e.target
    const searchParams = new URLSearchParams(location.search);
    searchParams.set(name, value);
    history.push(`${location.pathname}?${searchParams}`)
  }

  return (
    <Box className={classes.root}>
      <Box className={classes.listTop}>
        <div className="btn-list-grid">
          <Button className={viewType === 'grid' ? 'active' : ''} onClick={() => setViewType('grid')}><span className="ico-grid_view" /></Button>
          <Button className={viewType === 'list' ? 'active' : ''} onClick={() => setViewType('list')}><span className="ico-list" /></Button>
        </div>
        <div className="pagination-right">
          <div className="sort">
            <span>Sắp xếp theo</span>
            <select
              name="sort"
              value={order}
              onChange={handleSearch}
            >
              <option value="">{'Sắp xếp theo'}</option>
              <option value={'priceLowest'}>{'Giá thấp nhất'}</option>
              <option value={'priceHighest'}>{'Giá cao nhất'}</option>
            </select>
          </div>
          <div className="rows-per-page">
            <span>Hiển thị</span>
            <select
              name="limit"
              value={rowsPerPage}
              onChange={handleSearch}
            >
              <option value={12}>12</option>
              <option value={24}>24</option>
            </select>
          </div>
        </div>
      </Box>
      <Box className={classes.productList}>
        <Grid container spacing={4}>
          {list.length > 0 && (
            <>
              {list.map((item, index) => (
                <Grid key={index} item xs={viewType === 'grid' ? 4 : 12}>
                  <Product data={item} horizontal={viewType === 'list'} />
                </Grid>
              ))}
            </>
          )}
        </Grid>
      </Box>
      <Box display="flex" justifyContent="center" mt={4}>
        <Pagination
          color="primary"
          className={classes.pagination}
          defaultPage={defaultPage}
          count={totalPages}
          onChange={handleChangePage} />
      </Box>
    </Box>
  )
}

export default withRouter(List)