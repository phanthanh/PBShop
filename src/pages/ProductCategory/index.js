import React, { memo, useLayoutEffect, useState } from 'react';
import { Box, Container, Grid } from "@material-ui/core";
import Breadcrumb from 'components/Breadcrumb';
import SideBar from 'containers/SideBar';
import List from './List';
import { getProductCategory } from './crud';
import { Helmet } from 'react-helmet';

const ProductCategory = memo(function ({ match, history }) {
  const { categoryId } = match.params
  console.log('categoryId', categoryId);
  const [data, setData] = useState(null)

  useLayoutEffect(() => {
    if (categoryId) {
      getProductCategory(categoryId)
        .then(res => {
          if (res.status >= 200 && res.status < 300) {
            setData(res.data.data)
          } else {
            return history.push('/')
          }
        })
    } else {
      return history.push('/')
    }
  }, [categoryId])

  return (
    <Box>
      <Helmet>
        <title>{data?.name || 'Shop quần áo trẻ em'}</title>
      </Helmet>
      <Breadcrumb data={[
        { label: data?.name || '' }
      ]} />
      <Container>
        <Grid container spacing={4}>
          <Grid item xs={3}>
            <SideBar />
          </Grid>
          <Grid item xs={9}>
            <List />
          </Grid>
        </Grid>
      </Container>
    </Box>
  )
})

export default ProductCategory