import axios from 'axios'

const URL = 'api/product-categories'

export const getProductCategory = (categoryId) => {
    return axios.get(`${URL}/${categoryId}`)
}
