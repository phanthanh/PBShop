/**
 * Combine all reducers in this file and export the combined reducers.
 */

import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { reducer as formReducer } from "redux-form";
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import history from 'utils/history';
import authReducer from 'pages/Auth/reducer';
import checkoutReducer from 'pages/Checkout/reducer';

/**
 * Merges the main reducer with the router state and dynamically injected reducers
 */
export default function createReducer(injectedReducers = {}) {
  const rootReducer = combineReducers({
    auth: authReducer,
    checkout: checkoutReducer,
    form: formReducer,
    router: connectRouter(history),
    ...injectedReducers,
  });

  return persistReducer({
    key: 'root',
    storage: storage,
    whitelist: ['auth']
  }, rootReducer);
}
