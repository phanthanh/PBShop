import { applyMiddleware, compose, createStore } from "redux";
import { routerMiddleware } from 'connected-react-router';
import createSagaMiddleware from "redux-saga";
import { persistStore } from 'redux-persist';
import { actions as AuthActions } from "pages/Auth/reducer";
import createReducer from './reducers';

export default function configureStore(initialState = {}, history) {
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  const sagaMiddleware = createSagaMiddleware();
  const middlewares = [sagaMiddleware, routerMiddleware(history)];
  const store = createStore(
    createReducer(),
    initialState,
    composeEnhancers(applyMiddleware(...middlewares))
  );
  
  store.runSaga = sagaMiddleware.run;
  store.injectedReducers = {};
  store.injectedSagas = {};

  if (module.hot) {
    module.hot.accept('./reducers', () => {
      store.replaceReducer(createReducer(store.injectedReducers));
    });
  }

  const persistor = persistStore(store, null, () => {
    const auth = store.getState().auth
    if (auth.authenticated && new Date(auth.expiredAt) < new Date()) {
      store.dispatch(AuthActions.logout())
    }
  })

  return { store, persistor };
}
