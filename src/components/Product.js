import React, { memo } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Box, makeStyles } from "@material-ui/core";
import clsx from 'clsx';
import slugify from 'slugify';
import { formatVND } from 'utils/Util';
import { actions } from 'pages/Auth/reducer';
import AddToCart from 'containers/AddToCart';

const useStyle = makeStyles(theme => ({
  root: {
    width: '100%',
    height: '100%',
    background: '#f6f7f9',
    border: '1px solid #eee',
    display: 'flex',
    flexDirection: 'column',
    fontSize: theme.typography.fontSize,
    overflow: 'hidden',
    '& .product-desc': {
      color: theme.palette.common.black,
      padding: 10,
      textAlign: 'center',
      borderBottom: '1px solid #eee',
      '& h4': {
        marginBottom: theme.spacing(1),
      }
    },
    '& .media': {
      cursor: 'pointer',
      position: 'relative',
      display: 'flex',
      '& img': {
        width: '100%',
        transformStyle: 'preserve-3d',
        transition: 'all 0.7s ease 0s',
      },
      '& .product-img-extra': {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        opacity: 0,
        transform: 'scale(0, 1)',
      }
    },
    '& .caption': {
      background: theme.palette.common.white,
      marginTop: 'auto',
      transition: 'all 300ms',
      padding: 10,
      position: 'relative',
      '& .prices': {
        color: theme.palette.text.gray,
        fontSize: 18,
        fontWeight: 700,
        fontFamily: "'Baloo Paaji 2', cursive",
        transition: 'color 300ms ease-in-out',
        marginBottom: 0,
        '&.sale': {
          '& .base-price': {
            textDecoration: 'line-through',
            color: theme.palette.text.main,
          }
        },
        '& .sale-price': {
          marginLeft: theme.spacing(1),
        }
      },
      '& .MuiButtonBase-root, & .contact-us': {
        border: 'none',
        background: 'unset',
        boxShadow: 'unset',
        color: theme.palette.text.main,
        padding: 0,
        position: 'relative',
        transition: 'all 300ms ease-in-out',
        '&:before, & .ico-phone': {
          content: "'+'",
          position: 'absolute',
          left: 0,
          top: 0,
          color: theme.palette.common.white,
          opacity: 0,
          transition: 'all 0.5s',
        },
        '& .ico-phone': {
          top: 4,
        },
        '&:hover': {
          paddingLeft: 15,
          '&:before, & .ico-phone': {
            opacity: 1
          }
        }
      },
      '& .view-price': {
        cursor: 'pointer'
      },
      '& .contact-us': {
        '&:before': {
          display: 'none'
        },
      },
      '& .btn-group': {
        position: 'absolute',
        right: 5,
        top: 0,
        display: 'inline-block',
        verticalAlign: 'middle',
        transition: 'all 0.5s',
        transform: 'translateY(-50%)',
        '& button': {
          background: theme.palette.common.white,
          boxShadow: '0 0 6px 1px rgb(0 0 0 / 10%)',
          borderRadius: '50%',
          textAlign: 'center',
          opacity: 0,
          transform: 'rotateX(180deg)',
          transition: 'all 0.5s',
          margin: '0 2px',
          width: 35,
          height: 35,
          minWidth: 'unset',
          '& span': {
            color: theme.palette.text.gray
          }
        }
      }
    },
    '&:hover': {
      '& .media': {
        '& .product-img-extra': {
          opacity: 1,
          transform: 'scale(1, 1)',
        }
      },
      '& .caption': {
        background: theme.palette.primary.main,
        '& .prices, & .prices.sale .base-price, & .MuiButtonBase-root, & .contact-us, & .view-price': {
          color: theme.palette.common.white,
        },
        '& .btn-group': {
          '& button': {
            opacity: 1,
            transform: 'rotateX(0)',
          }
        }
      },
      '& .instock, & .outstock': {
        display: 'none',
      }
    },
    '& .instock': {
      color: '#2cc067',
    },
    '& .outstock': {
      color: '#f92020'
    },
  },
  horizontal: {
    flexDirection: 'row',
    background: theme.palette.common.white,
    border: 'none',
    height: 'auto',
    '& .media': {
      width: '33.3333334%',
      marginRight: theme.spacing(2),
      position: 'relative',
      '& .instock, & .outstock': {
        position: 'absolute',
        top: 10,
        left: 10,
        zIndex: 1,
        borderRadius: 20,
        padding: theme.spacing(.1, 1)
      },
      '& .instock': {
        background: '#2cc067',
        color: theme.palette.common.white,
        display: 'inline-block',
      }
    },
    '& .product-desc': {
      order: 1,
      padding: 0,
      '& h4': {
        color: theme.palette.text.main,
        marginBottom: theme.spacing(2),
      },
      color: theme.palette.text.gray,
      textAlign: 'left'
    },
    '& .caption': {
      padding: 0,
      width: '66.6666667%',
      display: 'flex',
      flexDirection: 'column',
      marginTop: 'unset',
      '& .btn-group': {
        position: 'relative',
        transform: 'unset',
        opacity: 1,
        order: 2,
        right: 0,
        margin: theme.spacing(1, -0.5),
        '& button': {
          opacity: 1,
          transform: 'unset',
          margin: theme.spacing(0, .5),
        }
      },
      '& .prices': {
        order: 2,
      }
    },
    '&:hover': {
      '& .caption': {
        background: 'unset',
        '& .price, & .price.sale .base-price, & .MuiButtonBase-root, & .contact-us': {
          color: theme.palette.text.main,
        },
      }
    }
  }
}))

const selectAuth = state => state.auth

export default memo(function ({
  data,
  horizontal,
}) {

  data.slug = slugify(data.name)
  const classes = useStyle()
  const history = useHistory()
  const dispatch = useDispatch()
  const auth = useSelector(selectAuth)

  return (
    <Box className={clsx([classes.root, horizontal ? classes.horizontal : ''])}>
      {!horizontal && (
        <Box className="product-desc">
          <h4 onClick={() => history.push(`/${data.slug}-p${data.id}`)}>{data.name}</h4>
          {/* <Rate /> */}
        </Box>
      )}
      <Box className="media" onClick={() => history.push(`/${data.slug}-p${data.id}`)}>
        {horizontal && <span className="instock"><span className="ico-check" /> Còn hàng</span>}
        {data.images?.length > 0 && data.images.map((image, idx) => <img key={idx} src={image} alt="" className={idx && "product-img-extra"} />)}
      </Box>
      <Box className="caption">
        {horizontal && (
          <Box className="product-desc">
            <h4 onClick={() => history.push(`/${data.slug}-p${data.id}`)}>{data.name}</h4>
            {/* <Rate /> */}
            <p>{data.description}</p>
          </Box>
        )}
        {/* <Box className="btn-group">
          <Button><span className="ico-eye" /></Button>
          <Button><span className="ico-heart" /></Button>
          <Button><span className="ico-stats-bars" /></Button>
          {horizontal && (
            <Button><span className="ico-local_grocery_store" /></Button>
          )}
        </Box> */}
        {/* add class sale with sale product */}
        <p className="prices">
          {auth.authenticated && (
            <>
              <span className="base-price">{formatVND(data.basePrice)}</span>
              {/* <span className="sale-price">12.12$</span> */}
            </>
          )}
        </p>
        <Box display="flex" justifyContent="space-between" alignItems="center">
          {!horizontal && (
            <>
              {auth.authenticated ? (
                <AddToCart product={data} quantity={1} label="Mua hàng" />
              ) : (
                  <span className="view-price" onClick={() => dispatch(actions.toggleLoginForm(true))}>Xem giá</span>
                )}
              <span className="instock"><span className="ico-check" /> Còn hàng</span>
            </>
          )}
          {/* <Button className="contact-us"><span className="ico-phone" />Liên hệ</Button>
          <span className="outstock">Hết hàng</span> */}
        </Box>
      </Box>
    </Box>
  )
})