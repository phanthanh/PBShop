import React, { memo } from "react";
import { Box, makeStyles } from "@material-ui/core";
import SwiperCore, { Navigation, Pagination } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import SliderPattern from '../assets/images/slider-pattern.png';

const BannerStyle = makeStyles(() => ({
  root: {
    overflow: 'hidden',
    position: 'relative',
    marginBottom: 70,
    height: 400,
    '&:after': {
      content: "''",
      width: "100%",
      height: 120,
      background: `url(${SliderPattern}) no-repeat center`,
      position: "absolute",
      left: 0,
      right: 0,
      textAlign: "center",
      bottom: "0px",
      zIndex: "1"
    }
  }
}))

SwiperCore.use([Navigation, Pagination]);

export default memo(() => {
  const classes = BannerStyle()
  return (
    <Box className={classes.root}>
      <Swiper
        spaceBetween={50}
        slidesPerView={1}
        navigation
        pagination={{ clickable: true }}
      >
        <SwiperSlide><img src="http://demo.ishithemes.com/opencart/OPC004/OPC004L01/image/cache/catalog/slider/slide-1-1920x750.png" alt="" /></SwiperSlide>
        <SwiperSlide><img src="http://demo.ishithemes.com/opencart/OPC004/OPC004L01/image/cache/catalog/slider/slide-2-1920x750.png" alt="" /></SwiperSlide>
      </Swiper>
    </Box>
  );
});
