/* eslint-disable no-use-before-define */
import React from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  option: {
    fontSize: 15,
    '& > span': {
      marginRight: 10,
      fontSize: 18,
    },
  },
});

export default function ({ list, label, input, meta, ...custom }) {
  const classes = useStyles();

  return (
    <Autocomplete
      style={{ width: '100%' }}
      options={list}
      classes={{
        option: classes.option,
      }}
      autoHighlight
      getOptionLabel={(option) => `${option.name}`}
      onChange={(e, option) => input.onChange(option?.id)}
      renderOption={(option) => (
        <React.Fragment>
          {option.name} ({option.code})
        </React.Fragment>
      )}
      renderInput={(params) => (
        <TextField
          {...params}
          label={label}
          variant="standard"
          {...meta}
          {...custom}
        />
      )}
    />
  );
}
