import { useState } from "react"
import { Box, Button, makeStyles } from '@material-ui/core'

const useStyle = makeStyles(theme => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    '& input': {
      border: '1px solid #DDD',
      height: 30,
      width: 40,
      textAlign: 'center',
      outline: 'unset',
    },
    '& button': {
      padding: 0,
      width: 30,
      height: 30,
      minWidth: 'unset',
      borderRadius: 'unset',
      color: theme.palette.common.white,
      boxShadow: 'unset',
    }
  }
}))

let delayChange

export default function ({ quantity, onChange, maxQty }) {
  const classes = useStyle()
  const [value, setValue] = useState(quantity || 0)

  const handleChange = e => {
    const { value } = e.target
    let newQty = quantity
    if (value === '' || Number(value)) {
      newQty = value > maxQty ? maxQty : (value || 1)
    }
    setValue(newQty)
    updateQty(newQty)
  }

  const handleUp = () => {
    let newQty = value + 1
    if (maxQty) {
      newQty = newQty > maxQty ? maxQty : newQty
    }
    setValue(newQty)
    updateQty(newQty)
  }

  const handleDown = () => {
    let newQty = value - 1
    newQty = newQty < 1 ? 1 : newQty
    setValue(newQty)
    updateQty(newQty)
  }

  function updateQty(newQty) {
    clearTimeout(delayChange)
    if (newQty !== quantity) {
      delayChange = setTimeout(() => {
        onChange(newQty)
      }, 300);
    }
  }

  return (
    <Box className={classes.root}>
      <Button onClick={handleDown} color="secondary" variant="contained">-</Button>
      <input type="tel" value={value} onChange={handleChange} onFocus={e => e.target.select()} />
      <Button onClick={handleUp} color="secondary" variant="contained">+</Button>
    </Box>
  )
}