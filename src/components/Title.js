import React from 'react';
import { Typography, makeStyles } from '@material-ui/core';

const useStyle = makeStyles(theme => ({
  root: {
    fontFamily: "'Baloo Paaji 2', cursive",
    color: "#000",
    fontWeight: 700,
    textTransform: "capitalize",
    '&.MuiTypography-h4': {
      fontSize: 14,
    },
    '&.MuiTypography-h2': {
      fontSize: 24,
      lineHeight: "28px",
    }
  },
  [theme.breakpoints.up(768)]: {
    root: {
      '&.MuiTypography-h4': {
        fontSize: 16,
      },
      '&.MuiTypography-h2': {
        fontSize: 28,
        lineHeight: "32px",
      }
    }
  },
  [theme.breakpoints.up(960)]: {
    root: {
      '&.MuiTypography-h2': {
        fontSize: 36,
        lineHeight: "40px",
      }
    }
  }
}))

const Title = ({ children, ...custom }) => {
  const classes = useStyle();
  return (
    <Typography
      variant="h1"
      component="h2"
      className={classes.root}
      {...custom}>
      {children}
    </Typography>
  )
}

export default Title