import React from 'react'
import TextField from '@material-ui/core/TextField'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(() => ({
  root: {
    width: '100%',
  }
}))

export default function ({ label, input, inputProps, meta: { touched, invalid, error }, multiline, rows, ...custom }) {
  const classes = useStyles()
  return (
    <TextField
      label={label}
      error={touched && invalid}
      helperText={touched && error}
      className={classes.root}
      {...input}
      {...custom}
      value={input.value || ''}
      multiline={multiline}
      rows={rows}
      inputProps={inputProps}
    />
  )
}
