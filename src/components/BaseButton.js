import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles(theme => ({
  root: {
    borderRadius: 30,
    '&.MuiButton-containedPrimary': {
      color: theme.palette.common.white,
      '&:hover': {
        backgroundColor: theme.palette.common.black,
        borderColor: theme.palette.common.black,
      }
    },
    '& .MuiButton-label': {
      width: 'auto',
      fontWeight: 400,
    }
  }
}))

const BlackButton = withStyles(theme => ({
  root: {
    color: props => props.variant === 'outlined' ? theme.palette.common.black : theme.palette.common.white,
    backgroundColor: props => props.variant === 'outlined' ? theme.palette.common.white : theme.palette.common.black,
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: theme.palette.common.black,
    borderRadius: 30,
    '&:hover': {
      backgroundColor: theme.palette.primary.main,
      borderColor: theme.palette.primary.main,
    },
  }
}))(Button)

export default function CustomizedButtons({ children, color, variant, initStyle, ...custom }) {

  const classes = useStyles({ variant })

  if (color === 'black') {
    return <BlackButton {...custom} variant={variant} style={initStyle}>{children}</BlackButton>
  }

  return <Button color={color} variant={variant} style={initStyle} className={classes.root} {...custom}>{children}</Button>
}