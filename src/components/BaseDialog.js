/* eslint-disable no-restricted-imports */
import React, { useEffect } from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import { Dialog, DialogContent } from "@material-ui/core";
import Slide from '@material-ui/core/Slide';
import IconButton from '@material-ui/core/IconButton';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import Typography from '@material-ui/core/Typography';

const mediaScreen = {
  hg: 1440,
  xl: 1140,
  lg: 960,
  md: 720,
  sm: 600,
  xs: 400
}

const useStyles = makeStyles(theme => ({
  dialog: {
    '& .SidebarDialog-Body': {
      paddingBottom: 60
    },
    '& .SidebarDialog-Footer': {
      position: 'absolute',
      bottom: 0,
      padding: 10,
      left: 0,
      right: 0,
      zIndex: 1,
      borderTop: '1px solid rgba(0, 0, 0, 0.23)',
      background: '#fff'
    },
    '& .loading': {
      position: 'absolute',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      zIndex: 99,
    }
  },
  content: {
    padding: theme.spacing(2, 2, 10),
    overflow: 'auto'
  },
  left: {
    justifyContent: 'flex-start',
  },
  right: {
    justifyContent: 'flex-end'
  },
  paper: {
    margin: 0,
    overflow: 'inherit',
    width: props => !props.fullScreen ? mediaScreen[props.size || 'sm'] : 'unset',
  }
}))

const titleStyles = theme => ({
  root: {
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '10px 10px 10px 20px',
    minHeight: 60,
    background: theme.palette.primary.main,
    color: '#fff',
    '& > h5': {
      fontWeight: 500,
      textTransform: 'uppercase',
      fontSize: 20,
      color: '#fff'
    }
  },
  closeButton: {
    position: 'absolute',
    top: 20,
    right: 15,
    color: theme.palette.grey[300],
    padding: 0,
    transition: 'color 300ms',
    '&:hover': {
      background: 'unset',
      color: theme.palette.grey[50],
    }
  },
})

const DialogTitle = withStyles(titleStyles)(props => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h5">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <span className="ico-close" />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
})

export default function ({ open, title, children, onClose, direction, size, disableBackdropClick, classesStyle, styleContent, ...custom }) {
  const classes = useStyles({ size })
  let customStyle = { paper: classes.paper, scrollPaper: classes[direction || 'down'] }

  useEffect(() => {
    const body = document.body
    open ? body.classList.add('sidebar-opening') : body.classList.remove('sidebar-opening')
  }, [open])

  return (
    <Dialog
      TransitionComponent={Transition}
      open={open}
      onClose={() => onClose(false)}
      classes={{ ...customStyle, ...classesStyle }}
      maxWidth={'xl'}
      className={classes.dialog}
      disableBackdropClick={disableBackdropClick}
      {...custom}
    >
      {custom.loading && <div className="loading" />}
      {title && (
        <DialogTitle onClose={() => onClose(false)}>
          {title}
        </DialogTitle>
      )}
      <DialogContent style={styleContent} className={classes.content}>
        {children}
      </DialogContent>
    </Dialog>
  )
}