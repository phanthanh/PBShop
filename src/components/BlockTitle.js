import React from 'react';
import { Box, makeStyles } from '@material-ui/core';
import Icon from '../assets/images/title-icon.png';
import Title from './Title';

const useStyle = makeStyles(theme => ({
  root: {
    textAlign: 'center',
    position: "relative",
    padding: "80px 0 0",
    margin: "0 0 25px",
    letterSpacing: "0.5px"
  },
  icon: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    width: 60,
    height: 60,
    textAlign: "center",
    margin: "0 auto",
    border: "1px solid #e4e4e4",
    padding: 5,
    borderRadius: "50%",
    background: `url(${Icon}) no-repeat center`,
    '&:before, &:after': {
      content: "''",
      height: 1,
      width: 70,
      background: "#e4e4e4",
      position: "absolute",
      left: -70,
      top: "46%"
    },
    '&:after': {
      left: 'auto',
      right: -70,
      top: '62%',
    },
    '& span': {
      '&:before, &:after': {
        content: "''",
        height: 10,
        width: 16,
        background: theme.palette.common.white,
        position: 'absolute',
        left: -12,
        top: 28,
      },
      '&:after': {
        right: -12,
        left: 'auto',
        top: 26,
      }
    }
  }
}))

const BlockTitle = ({ children, ...custom }) => {
  const classes = useStyle();
  return (
    <Box className={classes.root}>
      <span className={classes.icon}><span /></span>
      <Title {...custom}>{children}</Title>
    </Box>
  )
}

export default BlockTitle