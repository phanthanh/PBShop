import React, { memo } from 'react';
import { Link } from 'react-router-dom';
import { Box, makeStyles } from "@material-ui/core";

const useStyle = makeStyles(theme => ({
    root: {
        display: 'block',
        overflow: 'hidden',
        textAlign: 'center',
        margin: '0 auto',
        marginBottom: theme.spacing(3),
        background: 'url(http://demo.ishithemes.com/opencart/OPC004/OPC004L01/image/catalog/breadcrumb.jpg) no-repeat top',
        backgroundAttachment: 'fixed',
        backgroundColor: '#f7f7f7',
        '& ul': {
            padding: '80px 0',
            textAlign: 'center',
            display: 'inline-flex',
            '& .divider': {
                margin: theme.spacing(0, .5)
            }
        }
    }
}))

export default memo(function ({ data }) {
    const classes = useStyle()
    return (
        <Box className={classes.root}>
            <ul>
                <li><Link to="/"><span className="ico-home" /></Link></li>
                {data?.map(item => (
                    <>
                        <li className="divider">|</li>
                        <li>{item.url ? <Link to={`/${item.url}`}>{item.label}</Link> : <span>{item.label}</span>}</li>
                    </>
                ))}
            </ul>
        </Box>
    )
})
