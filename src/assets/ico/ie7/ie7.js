/* To avoid CSS expressions while still supporting IE 7 and IE 6, use this script */
/* The script tag referencing this file must be placed before the ending body tag. */

/* Use conditional comments in order to target IE 7 and older:
	<!--[if lt IE 8]><!-->
	<script src="ie7/ie7.js"></script>
	<!--<![endif]-->
*/

(function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'ico\'">' + entity + '</span>' + html;
	}
	var icons = {
		'ico-banknote': '&#xe935;',
		'ico-check': '&#xe935;',
		'ico-bill': '&#xe935;',
		'ico-money': '&#xe935;',
		'ico-cash': '&#xe935;',
		'ico-search': '&#xe908;',
		'ico-home': '&#xe909;',
		'ico-user-circle-o': '&#xe90a;',
		'ico-group': '&#xe90b;',
		'ico-users': '&#xe90b;',
		'ico-arrow-left': '&#xe90c;',
		'ico-arrow-right': '&#xe90d;',
		'ico-volume-control-phone': '&#xe900;',
		'ico-shopping_basket': '&#xe932;',
		'ico-add_shopping_cart': '&#xe933;',
		'ico-local_grocery_store': '&#xe934;',
		'ico-add_circle': '&#xe92d;',
		'ico-add': '&#xe92e;',
		'ico-delete_forever': '&#xe92f;',
		'ico-remove_circle_outline': '&#xe930;',
		'ico-remove_circle': '&#xe931;',
		'ico-do_not_disturb_on': '&#xe931;',
		'ico-favorite_outline': '&#xe925;',
		'ico-star_outline': '&#xe926;',
		'ico-star_half': '&#xe927;',
		'ico-bookmarks': '&#xe928;',
		'ico-stars': '&#xe929;',
		'ico-bookmark': '&#xe92a;',
		'ico-star': '&#xe92b;',
		'ico-favorite': '&#xe92c;',
		'ico-home1': '&#xe90e;',
		'ico-filter_list_alt': '&#xe90f;',
		'ico-view_list': '&#xe910;',
		'ico-list': '&#xe911;',
		'ico-table_rows': '&#xe912;',
		'ico-grid_view': '&#xe913;',
		'ico-check_circle': '&#xe914;',
		'ico-check1': '&#xe915;',
		'ico-clear': '&#xe916;',
		'ico-close': '&#xe916;',
		'ico-logout': '&#xe917;',
		'ico-keyboard_backspace': '&#xe918;',
		'ico-login': '&#xe919;',
		'ico-person_search': '&#xe91a;',
		'ico-person_remove': '&#xe91b;',
		'ico-person_remove_alt_1': '&#xe91b;',
		'ico-person_add_alt_1': '&#xe91c;',
		'ico-verified_user': '&#xe91d;',
		'ico-person': '&#xe91e;',
		'ico-arrow_drop_down': '&#xe91f;',
		'ico-keyboard_arrow_down': '&#xe920;',
		'ico-arrow_drop_up': '&#xe921;',
		'ico-keyboard_arrow_up': '&#xe922;',
		'ico-arrow_left': '&#xe923;',
		'ico-navigate_before': '&#xe924;',
		'ico-chevron_left': '&#xe924;',
		'ico-arrow_left1': '&#xe907;',
		'ico-arrow_right_alt': '&#xe901;',
		'ico-arrow_right': '&#xe902;',
		'ico-navigate_next': '&#xe903;',
		'ico-chevron_right': '&#xe903;',
		'ico-call': '&#xe904;',
		'ico-phone': '&#xe905;',
		'ico-local_phone': '&#xe905;',
		'ico-email': '&#xe906;',
		'ico-mail': '&#xe906;',
		'ico-markunread': '&#xe906;',
		'ico-local_post_office': '&#xe906;',
		'ico-library': '&#xe937;',
		'ico-cart': '&#xe93a;',
		'ico-coin-dollar': '&#xe93b;',
		'ico-credit-card': '&#xe93f;',
		'ico-location': '&#xe947;',
		'ico-location2': '&#xe948;',
		'ico-map': '&#xe94b;',
		'ico-map2': '&#xe94c;',
		'ico-mobile': '&#xe958;',
		'ico-quotes-left': '&#xe977;',
		'ico-quotes-right': '&#xe978;',
		'ico-stats-bars': '&#xe99c;',
		'ico-eye': '&#xe9ce;',
		'ico-heart': '&#xe9da;',
		'ico-play': '&#xea1c;',
		'ico-youtube': '&#xea9d;',
		'0': 0
		},
		els = document.getElementsByTagName('*'),
		i, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		c = el.className;
		c = c.match(/ico-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
}());
