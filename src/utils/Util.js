export const formatVND = (amount, currency = 'VND', suffix = '.') => {
    if (Number(amount)) {
        const value = currency === 'VND' ? parseInt(amount) : amount
        var re = '\\d(?=(\\d{3})+$)';
        return value.toString().replace(new RegExp(re, 'g'), `$&${suffix}`);
    }
    return 0
}

export const formatErrorResponse = (error) => {
    if (error.response) {
        return error.response
    } else if (error.request) {
        return error.request
    }
    return error.message
}

export default {
    formatVND,
}