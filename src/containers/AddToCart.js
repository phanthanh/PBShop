import React from 'react';
import { Button, makeStyles } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { actions } from 'pages/Checkout/reducer';

const useStyle = makeStyles(theme => ({
  root: {
    display: 'inline-block',
    verticalAlign: 'middle',
    width: 'auto',
    padding: '6px 12px',
    fontSize: 14,
    fontWeight: 500,
    color: theme.palette.common.white,
    background: theme.palette.common.black,
    transition: 'all 300ms',
    borderRadius: 50,
    border: '1px solid #000',
    boxShadow: '0 2px 4px 0 rgb(0 0 0 / 20%)',
    '&:hover': {
      borderColor: theme.palette.primary.main,
      backgroundColor: theme.palette.primary.main,
    }
  },
}))

const selectCheckout = state => state.checkout

function AddToCart({ onClick, product, quantity, label, custom }) {
  const classes = useStyle()
  const dispatch = useDispatch()
  const checkout = useSelector(selectCheckout)

  const handleAddToCart = () => {
    const exist = checkout.items.find(item => product.id === item.id)
    const data = exist ? checkout.items.map(item => product.id === item.id ? { ...item, quantity: item.quantity + Number(quantity) } : item) : [...checkout.items, { ...product, quantity: Number(quantity) }]
    dispatch(actions.updateCart(data))
    if (typeof onClick === 'function') {
      onClick()
    }
  }

  return (
    <Button onClick={handleAddToCart} className={classes.root} {...custom}>
      {label || `Thêm vào giỏ`}
    </Button>
  )
}

export default AddToCart
