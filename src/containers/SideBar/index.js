import React, { useLayoutEffect, useState } from 'react'
import { Box, makeStyles } from '@material-ui/core'
import slugify from 'slugify'
import { getSaleProducts } from './crud'
import { formatVND } from 'utils/Util'

const useStyles = makeStyles(theme => ({
  root: {
    marginBottom: 30,
    border: "1px solid #e9e9e9",
    overflow: "hidden",
    position: "relative",
    display: "block",
    '& .title': {
      border: "medium none",
      color: "#000",
      font: "400 16px/18px 'Poppins', sans-serif",
      letterSpacing: "1px",
      width: "100%",
      display: "block",
      margin: 0,
      cssFloat: "none",
      textAlign: "left",
      background: "#f5f5f5",
      padding: 15,
      textTransform: "uppercase"
    },
    '& .list': {
      '& ul li': {
        padding: theme.spacing(1, 2),
      },
      '& .product-slim': {
        padding: theme.spacing(2),
        borderBottom: '1px solid #eee',
        display: 'flex',
        '& .media': {
          width: 90,
          height: 90,
          marginRight: theme.spacing(2),
        },
        '& h4': {
          fontSize: 16,
          fontWeight: 500,
          marginBottom: theme.spacing(0.5)
        },
        '& a': {
          color: theme.palette.text.main,
        },
        '& .prices': {
          display: 'flex',
        },
        '& .basePrice, & .salePrice': {
          color: theme.palette.text.main,
          fontWeight: 600,
          '&.sale': {
            fontSize: '90%',
            textDecoration: 'line-through',
            color: theme.palette.text.gray
          }
        },
        '& .salePrice': {
          marginRight: theme.spacing(0.5)
        }
      }
    }
  },
}))

function SideBar() {
  const classes = useStyles()

  const [listSales, setListSales] = useState([])

  useLayoutEffect(() => {
    getSaleProducts()
      .then(res => {
        if (res?.status === 200) {
          setListSales(res.data.data)
        }
      })
  }, [])

  const handleFilterByCategory = () => {

  }

  return (
    <>
      <Box className={classes.root}>
        <h3 className="title">Danh mục</h3>
        <Box className="list">
          <ul>
            <li onClick={handleFilterByCategory}>Fashion (15)</li>
          </ul>
        </Box>
      </Box>
      <Box className={classes.root}>
        <h3 className="title">Hàng giảm giá</h3>
        <Box className="list">
          {listSales.map(product => {
            const slug = slugify(product.name)
            return (
              <div className="product-slim">
                <Box className="media">{product.images?.length && <img src={product.images[0]} />}</Box>
                <div className="desc">
                  <h4><Link to={`/${slug}-p${product.id}`}>{product.name}</Link></h4>
                  <div className="prices">
                    <p className="salePrice">{formatVND(product.basePrice)}</p>
                    <p className="basePrice sale">{formatVND(product.salePrice)}</p>
                  </div>
                </div>
              </div>
            )
          })}
        </Box>
      </Box>
    </>
  )
}

export default SideBar