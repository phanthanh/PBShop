import axios from 'axios'

export const getSaleProducts = () => {
    return axios.get(`/products/sales`)
}
