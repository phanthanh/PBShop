import React, { useState } from 'react'
import { connect } from 'react-redux'
import { useDispatch, useSelector } from 'react-redux'
import { Field, reduxForm, getFormValues } from 'redux-form'
import { Box, FormHelperText, makeStyles } from '@material-ui/core'
import BaseDialog from 'components/BaseDialog'
import BaseButton from 'components/BaseButton'
import ReduxTextField from 'components/ReduxTextField'
import { actions } from 'pages/Auth/reducer'
import { register } from 'pages/Auth/crud'
import { isPhone } from 'utils/validations'

const useStyles = makeStyles(theme => ({
  root: {
    '& .MuiFormControl-root': {
      margin: theme.spacing(1, 0)
    },
    '& .SidebarDialog-Footer': {
      display: 'flex',
      justifyContent: 'space-between'
    }
  }
}))

function validate(values) {
  const error = {}
  const requiredFields = ['username', 'fullName', 'password', 'rePassword']
  requiredFields.forEach(field => {
    if (!values[field] || (values[field] && !values[field].trim())) {
      error[field] = 'Không được bỏ trống'
    }
  });
  if (values.password && values.rePassword && values.password !== values.rePassword) {
    error.rePassword = 'Không trùng khớp với mật khẩu'
  }
  if (!isPhone(values.username)) {
    error.username = 'Không hợp lệ'
  }
  return error
}

const selectAuth = state => state.auth

function SignUpForm({
  handleSubmit,
  submitting,
  formValues,
}) {

  const classes = useStyles()
  const dispatch = useDispatch()
  const auth = useSelector(selectAuth)
  const [formError, setFormError] = useState('')

  const handleClose = () => {
    dispatch(actions.toggleRegisterForm(false))
  }

  const onSubmit = () => {
    register(formValues)
      .then(res => {
        if (res?.status >= 200 && res?.status < 300) {
          dispatch(actions.toggleRegisterForm(false))
          dispatch(actions.toggleLoginForm(true))
        }
        else {
          setFormError(res?.data?.message || 'Có lỗi xảy ra. Vui lòng thử lại sau')
        }
      })
  }

  return (
    <BaseDialog open={auth.openRegisForm} onClose={handleClose} title="Đăng ký">
      <form className={classes.root}>
        <Field
          type="tel"
          component={ReduxTextField}
          label="Số điện thoại *"
          name="phone"
          variant="outlined"
          autoFocus={true}
        />
        <Field
          component={ReduxTextField}
          label="Họ và tên *"
          name="fullName"
          variant="outlined"
        />
        <Field
          component={ReduxTextField}
          type="password"
          label="Mật khẩu *"
          name="password"
          variant="outlined"
        />
        <Field
          component={ReduxTextField}
          type="password"
          label="Nhập lại mật khẩu *"
          name="rePassword"
          variant="outlined"
        />
        {formError && <FormHelperText error>{formError}</FormHelperText>}
        <Box className="SidebarDialog-Footer">
          <BaseButton
            variant="contained"
            color="black"
            onClick={handleClose}
            disabled={submitting}
          >
            Đóng
          </BaseButton>
          <BaseButton
            variant="contained"
            color="primary"
            onClick={handleSubmit(onSubmit)}
            disabled={submitting}
          >
            Đăng ký
          </BaseButton>
        </Box>
      </form>
    </BaseDialog>
  )
}

const Form = reduxForm({
  form: 'RegisterForm',
  validate
})(SignUpForm)

export default connect(state => ({
  formValues: getFormValues('RegisterForm')(state),
}))(Form)