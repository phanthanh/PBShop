import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Container, Grid, Button, Menu, MenuItem } from "@material-ui/core";
import { actions } from "pages/Auth/reducer";
import { HeaderNavStyle } from "./style";

const selectAuth = state => state.auth

export default function HeaderNav() {
  const classes = HeaderNavStyle()
  const dispatch = useDispatch()
  const auth = useSelector(selectAuth)
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleSignIn = () => {
    dispatch(actions.toggleLoginForm(true))
    setAnchorEl(null);
  }

  const handleSignUp = () => {
    dispatch(actions.toggleRegisterForm(true))
    setAnchorEl(null);
  }

  const handleSignOut = () => {
    dispatch(actions.logout())
    setAnchorEl(null);
  }

  return (
    <div className={classes.root}>
      <Container>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Button onClick={handleClick}>
              {auth.authenticated ? (
                <>
                  <em>Chào, </em>
                  <span style={{ paddingLeft: 4 }}>{auth.userInfo.fullName || auth.userInfo.userame}</span>
                </>
              ) : 'Tài khoản'} <span className="ico-arrow_drop_down" />
            </Button>
            <Menu
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={handleClose}
              className={classes.dropdown}
            >
              {!auth.authenticated ? (
                <>
                  <MenuItem onClick={handleSignIn}><span className="ico-login" /> Đăng nhập</MenuItem>
                  <MenuItem onClick={handleSignUp}><span className="ico-person" /> Đăng ký</MenuItem>
                </>
              ) : (
                <>
                  <MenuItem onClick={handleClose}><span className="ico-person" /> Thông tin tài khoản</MenuItem>
                  <MenuItem onClick={handleClose}><span className="ico-cart" /> Lịch sử mua hàng</MenuItem>
                  <MenuItem onClick={handleSignOut}><span className="ico-logout" /> Đăng xuất</MenuItem>
                </>
              )}
            </Menu>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}
