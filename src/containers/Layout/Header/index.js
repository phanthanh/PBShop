import React, { useEffect } from "react";
import HeaderNav from "./HeaderNav";
import HeaderTop from "./HeaderTop";
import HeaderMenu from "./HeaderMenu";

function Header() {
  useEffect(() => {
    const link = document.createElement("link");
    link.rel = "stylesheet";
    link.href = "https://fonts.googleapis.com/css2?family=Baloo+Paaji+2:wght@400;700&family=Roboto:wght@400;500&display=swap";
    document.head.appendChild(link);
  }, []);

  return (
    <>
      <HeaderNav />
      <HeaderTop />
      <HeaderMenu />
    </>
  );
}

export default Header;
