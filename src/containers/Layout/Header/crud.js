import axios from 'axios'

export const getProductCategories = (params) => {
    return axios.get(`api/product-categories`, {
        params
    })
}