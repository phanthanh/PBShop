import React, { useLayoutEffect, useState } from "react";
import { Box, Container } from "@material-ui/core";
import { useDispatch, useSelector } from 'react-redux';
import { Link } from "react-router-dom";
import slugify from 'slugify';
import { actions } from "pages/Checkout/reducer";
import { HeaderMenuStyle } from "./style";
import { getProductCategories } from "./crud";

const selectCheckout = state => state.checkout

export default function HeaderMenu() {

  const classes = HeaderMenuStyle()
  const [menus, setMenus] = useState([])
  const dispatch = useDispatch()

  const checkout = useSelector(selectCheckout)
  const totalCart = checkout.items.length ? checkout.items.reduce((a, b) => a + Number(b.quantity), 0) : 0

  useLayoutEffect(() => {
    getProductCategories({ limit: 6 })
      .then(res => {
        if (res.status >= 200 && res.status < 300) {
          setMenus(res.data.data)
        }
      })
  }, [])

  const handleClick = () => {
    dispatch(actions.toggleCart(!checkout.showCart))
  }

  return (
    <div className={classes.root}>
      <Container>
        <Box className="wrap">
          <ul className="top-menu">
            {menus.map((menu, idx) => {
              const slug = slugify(menu.name)
              return (
                <li key={idx} className="top-level dropdown">
                  <Link to={`/${slug}/c${menu.id}`}>{menu.name}</Link>
                </li>
              )
            })}
            <li className="top-level dropdown">
              <Link to="">Bé Trai</Link>
              <div className="sub-menu">
                <ul>
                  <Link to="">Hằng ngày</Link>
                  <div className="dropdown-inner">
                    <ul>
                      <li><Link to="">Áo phông</Link></li>
                      <li><Link to="">Áo sơ mi</Link></li>
                      <li><Link to="">Quần jeans</Link></li>
                      <li><Link to="">Quần vải</Link></li>
                    </ul>
                  </div>
                </ul>
                <ul>
                  <Link to="">Hằng ngày</Link>
                  <div className="dropdown-inner">
                    <ul>
                      <li><Link to="">Áo phông</Link></li>
                      <li><Link to="">Áo sơ mi</Link></li>
                      <li><Link to="">Quần jeans</Link></li>
                      <li><Link to="">Quần vải</Link></li>
                    </ul>
                  </div>
                </ul>
                <ul>
                  <Link to="">Hằng ngày</Link>
                  <div className="dropdown-inner">
                    <ul>
                      <li><Link to="">Áo phông</Link></li>
                      <li><Link to="">Áo sơ mi</Link></li>
                      <li><Link to="">Quần jeans</Link></li>
                      <li><Link to="">Quần vải</Link></li>
                    </ul>
                  </div>
                </ul>
              </div>
            </li>
          </ul>
          <Box className="right-menu">
            <a href="tel:0977247006">
              <span className="ico-phone" />
              <span>097 724 7006</span>
            </a>
            <Box onClick={handleClick}>
              <span className="ico-cart" />
              <span className="divider"> | </span>
              <span className="quantity">{totalCart}</span>
              <span>sản phẩm</span>
            </Box>
          </Box>
        </Box>
      </Container>
    </div>
  );
}
