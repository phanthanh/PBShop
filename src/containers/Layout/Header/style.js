import { makeStyles } from "@material-ui/core";
import NavPattern from '../../../assets/images/nav-pattern.png';

export const HeaderNavStyle = makeStyles(theme => ({
  root: {
    position: 'relative',
    background: '#222',
    borderBottom: '1px solid',
    '&:after': {
      content: "''",
      height: "11px",
      width: "100%",
      background: `url(${NavPattern}) center`,
      position: "absolute",
      bottom: -9
    },
    '& button': {
      float: 'right',
      color: '#a8a8a8',
      textTransform: 'none'
    },
  },
  dropdown: {
    '& ul': {
      minWidth: 200,
      '& li': {
        '&:hover': {
          color: theme.palette.secondary.main
        },
        '& span': {
          marginRight: theme.spacing(1)
        }
      }
    }
  }
}))

export const HeaderTopStyle = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3, 0),
  },
  desc: {
    color: theme.palette.text.gray,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    '&:not(:last-child)': {
      marginRight: theme.spacing(2)
    },
    '& > div:nth-child(1)': {
      width: 55,
      height: 55,
      backgroundPosition: 'center',
      marginRight: theme.spacing(2),
      transition: 'all 300ms'
    },
    '& > div:nth-child(2)': {
      display: 'flex',
      flexDirection: 'column',
      '& .MuiTypography-h4': {
        transition: 'all 300ms'
      },
      '& p': {
        marginBottom: 0
      }
    },
    '&:hover': {
      '& > div:nth-child(1)': {
        transform: 'scale(1.1)',
      },
      '& .MuiTypography-h4': {
        color: theme.palette.secondary.main,
      }
    }
  }
}))

export const HeaderMenuStyle = makeStyles(theme => ({
  root: {
    background: theme.palette.primary.main,
    position: 'relative',
    zIndex: 2,
    '& .wrap': {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    '& .top-menu': {
      '& .top-level': {
        display: "inline-block",
        verticalAlign: "middle",
        position: "relative",
        cursor: "pointer",
        zIndex: "1",
        padding: theme.spacing(1, 0),
        '&:after': {
          content: "''",
          position: "absolute",
          top: theme.spacing(1),
          bottom: theme.spacing(1),
          left: 0,
          right: 0,
          zIndex: "-1",
          borderRadius: "20px",
          backgroundColor: "#ff6f69",
          transform: "scaleX(0)",
          transformOrigin: "100%",
          transitionProperty: "transform",
          transitionDuration: "0.5s",
          transitionTimingFunction: "ease-out"
        },
        '& > a': {
          color: theme.palette.common.white,
          fontFamily: "'Baloo Paaji 2', cursive",
          fontWeight: 700,
          fontSize: 18,
          display: 'inline-block',
          lineHeight: 1,
          padding: theme.spacing(1, 2),
          zIndex: 1,
          position: 'relative',
        },
        '& > .sub-menu': {
          display: 'flex',
          position: 'absolute',
          top: 50,
          left: 0,
          border: "none",
          width: "auto",
          zIndex: "18",
          padding: "15px 0",
          borderRadius: 0,
          margin: 0,
          transition: "all 0.3s",
          background: theme.palette.common.white,
          boxShadow: '0 5px 10px rgb(0 0 0 / 20%)',
          opacity: 0,
          transform: "scale(1, 0)",
          transformOrigin: "left top 0",
          '& > ul': {
            minWidth: 200,
            padding: theme.spacing(0, 2),
            '& > a': {
              padding: 0,
              paddingBottom: 4,
              marginBottom: 17,
              color: "#000",
              textTransform: "capitalize",
              fontSize: 15,
              position: "relative",
              display: "block",
              '&:after': {
                content: "''",
                borderTop: "1px solid #ddd",
                bottom: -7,
                position: "absolute",
                left: 0,
                width: "50%"
              },
              '&:hover': {
                color: theme.palette.secondary.main
              }
            }
          }
        },
        '&:hover': {
          '&:after': {
            transform: "scaleX(1)",
            transformOrigin: "0%",
          },
          '& > .sub-menu': {
            opacity: 1,
            transform: "scale(1, 1)",
          }
        },
        '& .dropdown-inner': {
          '& a': {
            display: 'block',
            padding: theme.spacing(0.5, 0),
            '&:hover': {
              color: theme.palette.secondary.main
            }
          }
        }
      }
    },
    '& .right-menu': {
      display: 'flex',
      alignItems: 'center',
      '& > *': {
        cursor: 'pointer',
        background: theme.palette.secondary.main,
        color: theme.palette.common.white,
        borderRadius: "20px",
        display: 'flex',
        alignItems: 'center',
        padding: '4px 10px 4px 8px',
        height: 34,
        '&:not(:last-child)': {
          marginRight: theme.spacing(2)
        },
        '& > span:first-child': {
          transition: 'all 0.5s',
          marginRight: theme.spacing(.5),
        },
        '& .divider': {
          margin: theme.spacing(0, .5),
        },
        '& .quantity': {
          borderRadius: "50%",
          height: 17,
          width: 17,
          lineHeight: '17px',
          display: "inline-block",
          verticalAlign: "middle",
          background: "#09ccd0",
          color: theme.palette.common.white,
          fontSize: 11,
          fontWeight: 500,
          textAlign: "center",
          margin: "0 2px 0 3px",
          letterSpacing: "0"
        },
        '&:hover': {
          '& > span:first-child': {
            transform: 'rotateY(360deg)'
          }
        }
      }
    }
  }
}))
