import React from "react";
import { Link } from "react-router-dom";
import { Container, Grid, Box } from "@material-ui/core";
import { HeaderTopStyle } from "./style";
import Logo from './Logo.jpg';
import DeliveryImg from '../../../assets/images/delivery.png';
import GiftImg from '../../../assets/images/gift.png';
import Title from '../../../components/Title';

export default function HeaderTop() {
  const classes = HeaderTopStyle()
  return (
    <div className={classes.root}>
      <Container>
        <Grid container spacing={2}>
          <Grid item md={5} sm={8} xs={7}>
            <Box>
              <Link to="/">
                <img src={Logo} alt="" height={70} />
              </Link>
            </Box>
          </Grid>
          <Grid item md={7}>
            <Box display="flex" alignItems="center" justifyContent="flex-end">
              <Box className={classes.desc}>
                <Box className="media" style={{ backgroundImage: `url(${DeliveryImg})` }} />
                <Box>
                  <Title variant="h4">Miễn Phí Vận Chuyển</Title>
                  <p>Giao hàng toàn quốc</p>
                </Box>
              </Box>
              <Box className={classes.desc}>
                <Box className="media" style={{ backgroundImage: `url(${GiftImg})` }} />
                <Box>
                  <Title variant="h4">Hỗ trợ 24/7</Title>
                  <p>7 Ngày Miễn Phí Trả Hàng</p>
                </Box>
              </Box>
            </Box>
          </Grid>
          <Grid item sm={4} xs={5} className="md-none">

          </Grid>
        </Grid>
      </Container>
    </div>
  );
}
