import React, { memo } from "react";
import { Link } from "react-router-dom";
import { Box, Container, Grid } from "@material-ui/core";
import Title from '../../../components/Title';
import { FooterStyle } from "./style";
import Logo from './Logo.png';

export default memo(() => {
  const classes = FooterStyle();
  return (
    <Box className={classes.root}>
      <Container>
        <Grid container spacing={2}>
          <Grid item md={3}>
            <Box>
              <img src={Logo} alt="" />
              <p>This is Photoshops version of Lorem fenIpsum. Proin gravida nibh sagitt sem nibh id elit.Proin gravida nibh sagitt sem nibh id elit.</p>
            </Box>
          </Grid>
          <Grid item md={3}>
            <Title variant="h4">Thông tin</Title>
            <ul>
              <li><Link to="">Chính sách vận chuyển</Link></li>
              <li><Link to="">Chính sách đổi trả hàng</Link></li>
              <li><Link to="">Liên hệ</Link></li>
            </ul>
          </Grid>
          <Grid item md={3}>
            <Title variant="h4">Tài khoản</Title>
            <ul>
              <li><Link to="">Chính sách vận chuyển</Link></li>
              <li><Link to="">Chính sách đổi trả hàng</Link></li>
              <li><Link to="">Liên hệ</Link></li>
            </ul>
          </Grid>
          <Grid item md={3}>
            <Title variant="h4">Cửa hàng</Title>
            <ul>
              <li><Link to="">Chính sách vận chuyển</Link></li>
              <li><Link to="">Chính sách đổi trả hàng</Link></li>
              <li><Link to="">Liên hệ</Link></li>
            </ul>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
});
