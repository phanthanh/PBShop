import { makeStyles } from "@material-ui/core";
import BgImg from './bg.jpg';
import Pattern from '../../../assets/images/footer-pattern.png';

export const FooterStyle = makeStyles(theme => ({
  root: {
    background: `#e8f6f7 url(${BgImg}) bottom no-repeat`,
    marginTop: 30,
    padding: theme.spacing(5, 0, 10),
    color: theme.palette.text.gray,
    lineHeight: '1.25em',
    position: 'relative',
    '&:before': {
      content: "''",
      height: 11,
      width: '100%',
      background: `url(${Pattern}) center`,
      position: 'absolute',
      top: 0,
    },
    '& .MuiTypography-h4': {
      color: theme.palette.secondary.main,
      textTransform: 'uppercase',
      marginBottom: theme.spacing(1),
    },
    '& ul > li > a': {
      display: 'block',
      margin: theme.spacing(1, 0),
      color: theme.palette.text.gray,
      '&:hover': {
        color: theme.palette.primary.main
      }
    }
  }
}))
