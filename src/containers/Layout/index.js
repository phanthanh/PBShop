import React, { memo, Suspense } from "react";
import { Helmet } from "react-helmet";
import { Route } from 'react-router-dom';
import SignInForm from "../SignInForm";
import SignUpForm from "../SignUpForm";
import Header from "./Header";
import Footer from "./Footer";
import Cart from "./Cart";

const Layout = memo(({ children }) => (
  <>
    <Helmet>
      <meta charSet="utf-8" />
      <title>Shop quần áo trẻ em</title>
    </Helmet>
    <Suspense fallback={'Loading...'}>
      <Header />
      {children}
      <Footer />
      <Cart />
      <SignInForm />
      <SignUpForm />
    </Suspense>
  </>
));

const CustomerLayoutRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(props) => (
        <Layout>
          <Component {...props} />
        </Layout>
      )}
    />
  );
};

export default CustomerLayoutRoute
