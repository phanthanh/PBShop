import React, { useEffect } from 'react';
import { useHistory } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import clsx from 'clsx';
import { createSelector } from 'reselect';
import { Box, makeStyles } from '@material-ui/core';
import { actions } from 'pages/Checkout/reducer';
import Button from 'components/BaseButton';
import { formatVND } from 'utils/Util';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(2),
    position: 'fixed',
    top: 0,
    right: '-100%',
    width: 320,
    height: '100vh',
    zIndex: 99,
    background: theme.palette.common.white,
    border: '1px solid #e9e9e9',
    transition: 'all 300ms',
    '& > button': {
      background: 'unset',
      border: 'none',
      fontSize: 30,
      cursor: 'pointer'
    }
  },
  show: {
    right: 0,
  },
  product: {
    display: 'flex',
    borderBottom: '1px solid #e9e9e9',
    marginTop: theme.spacing(1),
    paddingBottom: theme.spacing(1),
    '& img': {
      width: 60,
      marginRight: theme.spacing(1)
    },
    '& .prices': {
      fontSize: 16,
      fontFamily: "'Baloo Paaji 2', cursive",
      marginBottom: 0,
      display: 'flex',
      alignItems: 'center',
      '&.sale': {
        '& .base-price': {
          textDecoration: 'line-through',
          color: theme.palette.text.main,
        }
      },
      '& .sale-price': {
        marginLeft: theme.spacing(1),
      }
    },
  },
  footer: {
    '& p': {
      display: 'flex',
      justifyContent: 'space-between',
      padding: theme.spacing(1, 0),
      fontSize: 16
    }
  }
}))

const selectCheckout = state => state.checkout
const selectItems = state => state.items

function Cart() {

  const classes = useStyles()
  const checkout = useSelector(selectCheckout)
  const dispatch = useDispatch()
  const history = useHistory()

  useEffect(() => {
    history.listen(() => {
      dispatch(actions.toggleCart(false))
    })
  }, [history])

  const subtotalSelector = createSelector(
    selectItems,
    items => items.reduce((subtotal, item) => subtotal + (item.quantity * (item.salePrice || item.basePrice)), 0)
  )

  return (
    <Box className={clsx(classes.root, checkout.showCart ? classes.show : false)}>
      <button onClick={() => dispatch(actions.toggleCart(false))}><span className="ico-close" /></button>
      {checkout.items.length > 0 ? checkout.items.map(item => (
        <Box key={item.id} className={classes.product}>
          <img src={item.images ? item.images[0] : ''} />
          <Box>
            <span><strong>{item.name}</strong></span>
            <p className="prices">
              <span style={{ marginRight: 4 }}>{item.quantity} x </span>
              <span className="base-price">{formatVND(item.salePricePrice || item.basePrice)}</span>
            </p>
          </Box>
        </Box>
      )) : (
          <Box className={classes.empty}>
            Chưa có hàng hóa nào trong giỏ
          </Box>
        )}
      {checkout.items.length > 0 && (
        <>
          <Box className={classes.footer}>
            <p><span>Tổng</span><span>{formatVND(subtotalSelector(checkout))}</span></p>
          </Box>
          <Box display="flex" justifyContent="space-around">
            <Button variant="contained" color="primary" onClick={() => dispatch(actions.toggleCart(false))} style={{ width: 120 }}>Đóng</Button>
            <Button variant="contained" color="black" onClick={() => history.push('/thanh-toan')}>Thanh toán</Button>
          </Box>
        </>
      )}
    </Box>
  )
}

export default Cart