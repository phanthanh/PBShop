import React, { useState } from 'react'
import { connect } from 'react-redux'
import { useDispatch, useSelector } from 'react-redux'
import { Field, getFormValues, reduxForm } from 'redux-form'
import { Box, FormHelperText, makeStyles } from '@material-ui/core'
import BaseDialog from 'components/BaseDialog'
import BaseButton from 'components/BaseButton'
import ReduxTextField from 'components/ReduxTextField'
import { actions } from 'pages/Auth/reducer'
import { login } from 'pages/Auth/crud'

const useStyles = makeStyles(theme => ({
  root: {
    '& .MuiFormControl-root': {
      margin: theme.spacing(1, 0)
    },
    '& .SidebarDialog-Footer': {
      display: 'flex',
      justifyContent: 'space-between'
    }
  }
}))

function validate(values) {
  const error = {}
  if (!values.username) {
    error.username = 'Không được bỏ trống'
  }
  if (!values.password) {
    error.password = 'Không được bỏ trống'
  }
  return error
}

const selectAuth = state => state.auth

function SignInForm({
  formValues,
  handleSubmit,
  submitting,
}) {

  const classes = useStyles()
  const dispatch = useDispatch()
  const auth = useSelector(selectAuth)
  const [formError, setFormError] = useState('')

  const handleClose = () => {
    dispatch(actions.toggleLoginForm(false))
  }

  const onSubmit = () => {
    login(formValues)
      .then(res => {
        if (res?.status >= 200 && res?.status < 300) {
          dispatch(actions.loginSuccess(res.data))
        }
        else {
          setFormError(res?.data?.message || 'Có lỗi xảy ra. Vui lòng thử lại sau')
        }
    })
  }

  return (
    <BaseDialog open={auth.openLoginForm} onClose={handleClose} title="Đăng nhập">
      <form className={classes.root}>
        <Field
          type="tel"
          component={ReduxTextField}
          label="Tên đăng nhập *"
          name="username"
          variant="outlined"
          autoFocus={true}
        />

        <Field
          component={ReduxTextField}
          type="password"
          label="Mật khẩu *"
          name="password"
          variant="outlined"
        />
        {formError && <FormHelperText error>{formError}</FormHelperText>}
        <Box className="SidebarDialog-Footer">
          <BaseButton
            variant="contained"
            color="black"
            onClick={handleClose}
            disabled={submitting}
          >
            Đóng
          </BaseButton>
          <BaseButton
            variant="contained"
            color="primary"
            onClick={handleSubmit(onSubmit)}
            disabled={submitting}
          >
            Đăng nhập
          </BaseButton>
        </Box>
      </form>
    </BaseDialog>
  )
}

const Form = reduxForm({
  form: 'LoginForm',
  validate
})(SignInForm)

export default connect(() => state => ({
  formValues: getFormValues('LoginForm')(state)
}))(Form)