import React, { memo, Suspense } from "react";
import { Link, Route } from "react-router-dom";
import { Helmet } from "react-helmet";
import { Layout, Menu } from 'antd';
import 'antd/dist/antd.css';

const { Header, Sider, Content } = Layout;
const { SubMenu } = Menu;

const AdminLayout = memo(({ children }) => (
  <>
    <Helmet>
      <meta charSet="utf-8" />
      <title>Admin Daulacshop</title>
    </Helmet>
    <Suspense fallback={'Loading...'}>
      <Header></Header>
      <Layout>
        <Sider>
          <Menu
            style={{ width: 256 }}
            defaultSelectedKeys={['1']}
            defaultOpenKeys={['sub1']}
            mode="inline"
          >
            <SubMenu key="sub1" title="Giao dịch">
              <Menu.Item key="1"><Link to="/admin/order">Đặt hàng</Link></Menu.Item>
            </SubMenu>
          </Menu>
        </Sider>
        <Content>{children}</Content>
      </Layout>
    </Suspense>
  </>
));

const AdminLayoutRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(props) => (
        <AdminLayout>
          <Component {...props} />
        </AdminLayout>
      )}
    />
  );
};

export default AdminLayoutRoute
