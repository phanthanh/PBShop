import React from 'react';
import ReactDOM from 'react-dom';
import { ThemeProvider } from '@material-ui/core/styles';
import theme from './config/theme';
import history from './utils/history';
import setupAxios from './config/axios';
import reportWebVitals from './reportWebVitals';
import configureStore from './store';
import App from './App';

const { store, persistor } = configureStore({}, history);

setupAxios(store)

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <App store={store} persistor={persistor} />
  </ThemeProvider>,
  document.getElementById('root')
);

reportWebVitals(console.log);
