import { createTheme, responsiveFontSizes } from '@material-ui/core/styles';

let theme = createTheme({
    breakpoints: {},
    palette: {
        primary: {
            main: '#ffd576',
        },
        secondary: {
            main: '#ff6f69',
        },
        text: {
            main: '#333',
            gray: '#777',
        }
    },
    typography: {
        body1: {
            margin: 0,
            fontSize: 14
        },
        body2: {
            margin: 0
        },
    }
});

theme = responsiveFontSizes(theme);

export default theme;