import axios from 'axios'
import { actions as AuthActions } from "pages/Auth/reducer";

const API_URL = 'http://localhost:3001'

export default function (store) {
  axios.interceptors.request.use(
    config => {
      const state = store.getState()
      const auth = state.auth
      if (auth.authenticated && new Date(auth.expiredAt) < new Date()) {
        axios.post(`${API_URL}/refresh-token`, {
          refreshToken: auth.refreshToken
        })
          .then(res => {
            config.headers.Authorization = `Bearer ${res.data.access_token}`;
            store.dispatch(AuthActions.loginSuccess(res.data))
          })
          .catch(err => {
            console.log(err)
            store.dispatch(AuthActions.logout())
          })
      } else {
        config.headers.Authorization = `Bearer ${auth.accessToken}`;
      }
      if (config.url && config.url.indexOf('http') < 0) {
        config.url = `${API_URL}/${config.url}`
      }
      return config;
    },
    err => Promise.reject(err)
  );
}