import React, { lazy } from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter, Switch } from "react-router-dom";
import { PersistGate } from 'redux-persist/integration/react';
import CustomerLayoutRoute from "./containers/Layout";
import AdminLayoutRoute from "./containers/AdminLayout";
import 'swiper/swiper.min.css';
// import 'swiper/components/navigation/navigation.scss';
// import 'swiper/components/pagination/pagination.scss';
import './assets/scss/style.scss';
import './assets/ico/style.scss';

const Home = lazy(() => import("./pages/Home"));
const ProductDetail = lazy(() => import('./pages/ProductDetail'));
const ProductSearch = lazy(() => import('./pages/ProductSearch'));
const ProductCategory = lazy(() => import('./pages/ProductCategory'));
const CheckoutPage = lazy(() => import('./pages/Checkout'));

// For Admin
const Dashboard = lazy(() => import('./pages/Dashboard'));
const AdminLogin = lazy(() => import('./pages/AdminLogin'));
const ManagerOrder = lazy(() => import('./pages/ManagerOrder'));

function App({ store, persistor }) {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <BrowserRouter>
          <Switch>
            <CustomerLayoutRoute exact path="/" component={Home} />
            <CustomerLayoutRoute path="/tim-kiem" component={ProductSearch} />
            <CustomerLayoutRoute path="/thanh-toan" component={CheckoutPage} />
            <CustomerLayoutRoute path="/:productSlug-p:productId" component={ProductDetail} />
            <CustomerLayoutRoute path="/:categorySlug/c:categoryId" component={ProductCategory} />
            <AdminLayoutRoute path="/admin/dashboard" component={Dashboard} />
            <AdminLayoutRoute path="/admin/login" component={AdminLogin} />
            <AdminLayoutRoute path="/admin/order" component={ManagerOrder} />
          </Switch>
        </BrowserRouter>
      </PersistGate>
    </Provider>
  );
}

export default App
